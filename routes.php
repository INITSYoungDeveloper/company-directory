<?php
// User Registration
$router->get('auth/register','RegistrationController@userRegistration'); //User Registration form
$router->post('auth/register','RegistrationController@userRegistrationStorage'); //Validate and store user info
// User Login
$router->get('auth/login','LoginController@userLogin');
$router->post('auth/login','LoginController@userLoginAuthentication');
// Google login
$router->get('auth/google/login','LoginController@userLoginWithGoogle');
$router->get('auth/google/login/verify','LoginController@userLoginWithGoogleVerification');
// User Forgot Password
$router->get('auth/lost_password','LoginController@forgotPassword');
$router->post('auth/lost_password','LoginController@updateUserWithPassword');
// User logout validation
$router->post('auth/logout','LogoutController@index');
// User Homepage
$router->get('auth/home','UsersController@home');

// User create company account
$router->post('companies/create','CompaniesController@createAccount');

// User Companies
$router->get('auth/companies','CompaniesController@showUserCompanies');

// Company template page
$router->get('companies/index','CompaniesController@index');

// Create company categories
$router->post('companies/categories/create','CompaniesController@createCompanyCategories');
// Delete Category
$router->post('companies/category/delete','CompaniesController@deleteCompanyCategories');
// Company dashboard page
$router->get('companies/dashboard','CompaniesDashboardController@index');

// Projects
$router->get('companies/projects','CompanyProjectsController@index');

// Create Project
$router->get('companies/project/create','CompanyProjectsController@createProject');
$router->post('companies/project/create','CompanyProjectsController@validateRequestData');

// View project
$router->get('projects/index','CompanyProjectsController@showProjectInfo');

$router->post('companies/projects/updates','CompanyProjectsController@updateProjectIfCompleted');

// Delete Company
$router->post('companies/delete','CompaniesController@deleteCompany');
// Edit Company
$router->get('companies/edit','CompaniesController@editCompanyInfo');
$router->post('companies/edit','CompaniesController@updateCompanyInfo');
// Error 404
$router->get('error/404','ErrorsController@Error404');

// PM Fields
$router->get('project/m','ProjectManagersController@index');    //Get PM Info
$router->get('project/m/create','ProjectManagersController@create');    //PM Form
$router->post('project/m/create','ProjectManagersController@storePMInfo');   //Store pm
$router->get('project/m/edit','ProjectManagersController@editPMInfo');  //Edit PM Info
$router->post('project/m/edit','ProjectManagersController@updatePMInfo');
$router->post('project/m/delete','ProjectManagersController@deletePM');
$router->post('projects/issues/create','CompanyProjectsController@createTask');
$router->get('projects/issue/update','CompanyProjectsController@updateTask');
$router->get('projects/issue/delete','CompanyProjectsController@deleteTask');
