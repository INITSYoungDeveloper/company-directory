<!doctype html>
<html lang="en">
<head>
	<meta charset="utf-8" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
	<title>Company Dir</title>

	<meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
    <meta name="viewport" content="width=device-width" />
    <!-- <script defer src="https://use.fontawesome.com/releases/v5.3.1/js/all.js"></script> -->
	<!--     Fonts and icons     -->
    <link href="http://netdna.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.css" rel="stylesheet">

	<!-- CSS Files -->
    <link href="/Public/assets/css/bootstrap.min.css" rel="stylesheet" />
    <link href="/Public/assets/css/gsdk-bootstrap-wizard.css" rel="stylesheet" />
    <link rel="stylesheet" href="../Public/css/loader.css">
    <!-- <script src="/Public/assets/js/vue.js"></script> -->
	<!-- CSS Just for demo purpose, don't include it in your project -->
    <link href="/Public/assets/css/demo.css" rel="stylesheet" />
    <style>
        .field::-webkit-scrollbar{
            display:none;
        }
        .pro-top{
            display:grid;
            grid-template-columns:auto auto;
            width:100%;
        }
        .pro-top:nth-child(1){
            text-align:right;

        }
        .pro-top:nth-child(2){
            text-align:right;
        }
        .f-page::-webkit-scrollbar{
            display:none;
        }
        .f-page{
            flex-wrap: nowrap;
            overflow-y: auto;
            overflow-x: hidden;
            -webkit-overflow-scrolling: touch;
            scroll-behavior: smooth;
            height:350px;
        }
        .company-files::-webkit-scrollbar{
            display:none;
        }
        .company-files{
            flex-wrap: nowrap;
            overflow-y: auto;
            overflow-x: hidden;
            -webkit-overflow-scrolling: touch;
            scroll-behavior: smooth;
            height:300px;
        }
    </style>
</head>

<body>


<div id="pager">
<div class="image-container set-full-height wizard-card" style="background: linear-gradient(rgba(1, 1, 48, 0.664),rgba(1, 1, 48, 0.856)),url('/Public/gif/6.gif');">
<form action="/company/register" method="POST" enctype="multipart/form-data">
    <!--   Big container   -->
    <div class="container">
      <div class="row">
        <div class="col-sm-8 col-sm-offset-2">

            <!--      Wizard container        -->
            <div class="wizard-container">

                <div style="border-radius:0px;" class="card wizard-card" data-color="blue" id="wizardProfile">
                    
                <!--        You can switch ' data-color="orange" '  with one of the next bright colors: "blue", "green", "orange", "red"          -->

                    	<div class="wizard-header">
                        	<h3>
                        	   <b>BUILD</b> YOUR PROFILE <br>
                        	   <small>This information will let us know more about you.</small>
                        	</h3>
                    	</div>

						<div class="wizard-navigation">
							<ul>
	                            <li><a href="#about" data-toggle="tab">About</a></li>
	                            <li><a href="#account" data-toggle="tab">Account</a></li>
                                <li><a href="#address" data-toggle="tab">Address</a></li>
                                <li><a href="#company" data-toggle="tab">Company</a></li>
	                        </ul>

						</div>

                        <div class="tab-content">
                            <div class="tab-pane" id="about">
                              <div class="row">
                                  <h4 class="info-text"> Let's start with the basic information (with validation)</h4>
                                  <div class="col-sm-4 col-sm-offset-1">
                                     <div class="picture-container">
                                          <div class="picture">
                                              <img src="/Public/assets/img/default-avatar.png" class="picture-src" id="wizardPicturePreview" title=""/>
                                              <input type="file" id="wizard-picture" name="user_img">
                                          </div>
                                          <h6>Choose Picture</h6>
                                      </div>
                                  </div>
                                  <div class="col-sm-6">
                                      <div class="form-group">
                                        <label>Full Name <small>(required)</small></label>
                                        <input name="user_name" type="text" class="form-control" placeholder="Blessing...">
                                      </div>
                                      <div class="row">
                                          <div class="col-sm-6">
                                            <div class="form-group">
                                                <label>Phone <small>(required)</small></label>
                                                <input name="user_phone" type="text" class="form-control" placeholder="0707995556...">
                                            </div>
                                          </div>
                                          <div class="col-sm-6">
                                            <div class="form-group">
                                                <label>Gender<small>(required)</small></label>
                                                <select name="user_gender" class="form-control">
                                                <option value="Male"> Male </option>
                                                <option value="Female"> Female </option>
                                                
                                            </select>
                                            </div>
                                          </div>
                                      </div>
                                      
                                  </div>
                                  <div class="col-sm-10 col-sm-offset-1">
                                      <div class="form-group">
                                          <label>Email <small>(required)</small></label>
                                          <input name="user_email" type="email" class="form-control" placeholder="andrew@creative-tim.com">
                                      </div>
                                  </div>
                              </div>
                            </div>
                            <div class="tab-pane" id="account">
                                <h4 class="info-text"> What are you doing? (checkboxes) </h4>
                                <div class="row">

                                    <div class="field col-sm-10 col-sm-offset-1" style="overflow:auto;display:flex;">
                                        <div class="col-sm-4">
                                            <div class="choice" data-toggle="wizard-checkbox">
                                                <input type="checkbox" name="job" value="SEO">
                                                <div class="icon">
                                                <i class="fa fa-server"></i>
                                                </div>
                                                <h6>SEO</h6>
                                            </div>
                                        </div>
                                        <div class="col-sm-4">
                                            <div class="choice" data-toggle="wizard-checkbox">
                                                <input type="checkbox" name="job" value="Design">
                                                <div class="icon">
                                                    <i class="fa fa-pencil"></i>
                                                </div>
                                                <h6>Design</h6>
                                            </div>
                                        </div>
                                        <div class="col-sm-4">
                                            <div class="choice" data-toggle="wizard-checkbox">
                                                <input type="checkbox" name="job" value="Business Administrator">
                                                <div class="icon">
                                                <i class="fa fa-business-time"></i>
                                                </div>
                                                <h6>Business Administrator</h6>
                                            </div>
                                        </div>
                                        <div class="col-sm-4">
                                            <div class="choice" data-toggle="wizard-checkbox">
                                                <input type="checkbox" name="job" value="Code">
                                                <div class="icon">
                                                    <i class="fa fa-terminal"></i>
                                                </div>
                                                <h6>Code</h6>
                                            </div>

                                        </div>
                                        <div class="col-sm-4">
                                            <div class="choice" data-toggle="wizard-checkbox">
                                                <input type="checkbox" name="job" value="Develop">
                                                <div class="icon">
                                                    <i class="fa fa-laptop"></i>
                                                </div>
                                                <h6>Develop</h6>
                                            </div>

                                        </div>
                                    </div>

                                </div>
                            </div>
                            <div class="tab-pane" id="address">
                                <div class="row">
                                    <div class="col-sm-12">
                                        <h4 class="info-text"> Are you living in a nice area? </h4>
                                    </div>
                                    <div class="col-sm-7 col-sm-offset-1">
                                         <div class="form-group">
                                            <label>Street Name</label>
                                            <input type="text" name="user_street_name" class="form-control" placeholder="5h Avenue">
                                          </div>
                                    </div>
                                    <div class="col-sm-3">
                                         <div class="form-group">
                                            <label>Street Number</label>
                                            <input type="text" name="user_street_number" class="form-control" placeholder="242">
                                          </div>
                                    </div>
                                    <div class="col-sm-5 col-sm-offset-1">
                                         <div class="form-group">
                                            <label>City</label>
                                            <input type="text" name="user_city" class="form-control" placeholder="New York...">
                                          </div>
                                    </div>
                                    <div class="col-sm-5">
                                         <div class="form-group">
                                            <label>Country</label><br>
                                             <select name="user_country" class="form-control">
                                                <option value="Afghanistan"> Afghanistan </option>
                                                <option value="Albania"> Albania </option>
                                                <option value="Algeria"> Algeria </option>
                                                <option value="American Samoa"> American Samoa </option>
                                                <option value="Andorra"> Andorra </option>
                                                <option value="Angola"> Angola </option>
                                                <option value="Anguilla"> Anguilla </option>
                                                <option value="Antarctica"> Antarctica </option>
                                                <option value="Nigeria"> Nigeria </option>
                                                <option value="Ghana"> Ghana </option>
                                                <option value="...">...</option>
                                            </select>
                                          </div>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane" id="company">
                              <div class="row">
                                  <h4 class="info-text"> Let's start with the basic information (with validation)</h4>
                                  <div class="col-sm-4 col-sm-offset-1">
                                     <h4 style="text-align:center;"> Upload Files</h4>
                                      <h1 class="h1 file-uploads" style="text-align:center;">
                                      <i class="fa fa-files-o"></i>
                                        F
                                      </h1>
                                  </div>
                                  <div class="col-sm-6">
                                      <div class="form-group">
                                        <label>
                                            Company Name 
                                            <small>(required)</small>
                                        </label>
                                        <input name="company_name" type="text" class="form-control" placeholder="INITS LIMITED...">
                                      </div>
                                      <div class="form-group">
                                        <label>
                                            Created By: 
                                            <small>
                                                (required)
                                            </small>
                                        </label>
                                        <input name="created_by" type="text" class="form-control" placeholder="Andrew...">
                                      </div>
                                      <div class="row">
                                          <div class="col-sm-6">
                                            <div class="form-group">
                                                <label>Phone <small>(required)</small></label>
                                                <input name="company_phone" type="text" class="form-control">
                                            </div>
                                          </div>
                                          <div class="col-sm-6">
                                            <div class="form-group">
                                                <label>Registered On:<small>(required)</small></label>
                                                <input name="registered_on" type="date" class="form-control">
                                            </div>
                                          </div>
                                      </div>
                                      
                                  </div>
                                  <div class="col-sm-10 col-sm-offset-1">
                                    <div class="form-group">
                                        <label>Location <small>(required)</small></label>
                                        <input name="company_location" type="text" class="form-control" placeholder="andrew@creative-tim.com">
                                    </div>
                                  </div>
                                  <div class="col-sm-10 col-sm-offset-1">
                                      <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>Email <small>(required)</small></label>
                                                <input name="company_email" type="email" class="form-control" placeholder="andrew@creative-tim.com">
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>Website <small>(required)</small></label>
                                                <input name="company_website" type="text" class="form-control" placeholder="andrew@creative-tim.com">
                                            </div>
                                        </div>
                                      </div>
                                  </div>
                                  <div class="col-sm-10 col-sm-offset-1">
                                    
                                    <div class="form-control" id="show-project">
                                        <!-- <label for="">Include Completed Projects</label> -->
                                        <a class="btn btn-sm btn-primary show-project" id="show-project" style="width:100%;">Include Project</a>
                                    </div>
                                  </div>
                              </div>
                            </div>
                        </div>
                        <div class="wizard-footer height-wizard">
                            <div class="pull-right">
                                <input type='button' class='btn btn-next btn-fill btn-primary btn-wd btn-sm' name='next' value='Next' />
                                <button class='btn btn-finish btn-fill btn-primary btn-wd btn-sm' name='finish'>Finish</button>

                            </div>

                            <div class="pull-left">
                                <input type='button' class='btn btn-previous btn-fill btn-default btn-wd btn-sm' name='previous' value='Previous' />
                            </div>
                            <div class="clearfix"></div>
                        </div>

                    
                </div>
            </div> <!-- wizard container -->
        </div>
      </div><!-- end row -->
    </div> <!--  big container -->
    <div class="container files" style="display:none;position:absolute;top:0px;bottom:0px;right:0px;left:0px;margin:100px;height:70%;color:white;background:linear-gradient(rgba(1, 1, 48, 1.664),rgba(1, 1, 48, 0.856));width:50%;margin:auto;">
        <div class="row">
            <div class="card" style="font-size:30px;padding:10px;font-weight:bolder;text-align:right;background:silver;color:black;">
                <div id="close">
                    <i class="fa fa-times-circle"></i>
                    X
                </div>
            </div>
            <div class="multi-file">
                <h1 class="h3">FILE</h1>
                <div class="company-files">
                
                <div class="increment">
                    <div class="input-group control-group" style="">
                    <div class="for-control">
                        <input type="file" name="company_file[]" class="form-control">
                    </div> 
                        <div class="input-group-btn">
                        <button class="btn-success btn add-c-file"  style="font-weight:bold;" type="button"><i class="fa fa-plus"></i>Add</button>
                        </div>
                    </div>
                </div>
                <div class="clone hide" style="display:none;">
                    <div class="control-group input-group" style="margin-top:10px">
                        <div class="form-control">
                        <input type="file" name="company_file[]" class="">
                        </div>
                        <div class="input-group-btn">
                            <button class="btn btn-danger remove-c-file"  style="font-weight:bold;" type="button"><i class="fa fa-minus"></i>Remove</button>
                        </div>
                    </div>
                </div>
                
                </div>
            </div>
        </div>

    </div>
    <div class="container files-2" style="display:none;position:absolute;top:0px;bottom:0px;right:0px;left:0px;margin:100px;height:80%;color:white;background:linear-gradient(rgba(1, 1, 48, 1.664),rgba(1, 1, 48, 0.856));width:50%;margin:auto;">
        <div class="row">
            <div class="card" style="font-size:30px;padding:10px;font-weight:bolder;text-align:right;background:silver;color:black;">
                <div id="close2">
                    <i class="fa fa-times-circle"></i>
                    X
                </div>
            </div>
            <div id="pro" class="multi-file" >
                <h1 class="h3 pro-top">
                    <div>
                        PROJECT 
                    </div>
                    <div v-on:click="increase" class="addProject"><i class="fa fa-plus-circle"></i></div>
                </h1>
                <hr>
                <div class="f-page">
                    <div class="row pro-inc" style="height:400px;">         
                        <div class="col-sm-12">
                            <div class="form-group">
                                <label for="">Project Name</label>
                                <input type="text" class="form-control" name="project_name">
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <div class="form-group">
                                <label for="">About The Project</label>
                                <input type="text" name="about_project" class="form-control">
                            </div>
                        </div>
                        <div class="col-md-12">
                            <hr>
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label for="">Project Starting Date</label>
                                    <input type="date" name="project_starting_date" class="form-control">
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label for="">Project Deadline Date</label>
                                    <input name="project_deadline_date" type="date" class="form-control">
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label for="">Project Presentation Date</label>
                                    <input name="project_presentation_date" type="date" class="form-control">
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <div class="form-group">
                                <label for="">Discussions</label>
                                <textarea name="discussions" class="form-control" id="" rows="4"></textarea>
                            </div>
                        </div>
                        <!-- Project File Uploads -->
                        <div class="col-md-12">
                            <label for="">Upload Project Files</label>
                            <div class="inc">
                                <div class="input-group cont-group" style="">
                                <div class="for-control">
                                    <input type="file" name="project_file[]" class="form-control">
                                </div>
                                    <div class="input-group-btn">
                                        <button class="btn-success btn add-p-file" style="font-weight:bold;" type="button">
                                            <i class="fa fa-plus"></i>Add
                                        </button>
                                    </div>
                                </div>
                            </div>
                            <div class="p-file" style="display:none;">
                                <div class="input-group main-r cont-group" style="margin-top:10px">
                                    <div class="form-control">
                                        <input type="file" name="project_file[]" class="">
                                    </div>
                                    <div class="input-group-btn">
                                        <button class="btn btn-danger remove-p-file" style="font-weight:bold;" type="button"><i class="fa fa-minus"></i>Remove</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Secont Pro -->
                    <div class="col-md-12 form-group">
                        <br><br>
                        <div class="form-control" id="pro-done">
                            <!-- <label for="">Include Completed Projects</label> -->
                            <a class="btn btn-sm btn-primary btn-fill" id="pro-done" style="width:100%;font-weight:bolder;font-size:20px;">
                                DONE
                            </a>
                        </div>
                        <br>
                    </div>
                </div>  <!-- End of the proj-->
            </div>
        </div>
    </div>
    </form>
</div>
</div>
<div class="" id="loaders" style="position:absolute;top:0px;bottom:0px;right:0px;left:0px;margin:100px;height:50%;color:white;background:white;width:50%;margin:auto;">

<div class="loaders" >
    <span></span>
    <span></span>
    <span></span>
    <span></span>
</div>
    </div>
</body>
	<!--   Core JS Files   -->
	<script src="/Public/assets/js/jquery-2.2.4.min.js" type="text/javascript"></script>
	<script src="/Public/assets/js/bootstrap.min.js" type="text/javascript"></script>
	<script src="/Public/assets/js/jquery.bootstrap.wizard.js" type="text/javascript"></script>

	<!--  Plugin for the Wizard -->
	<script src="/Public/assets/js/gsdk-bootstrap-wizard.js"></script>

	<!--  More information about jquery.validate here: http://jqueryvalidation.org/	 -->
	<script src="/Public/assets/js/jquery.validate.min.js"></script>
    <script>
        $(function(){
            // $('#pager').slideToggle('fast');
            $("#loaders").fadeOut(1500,function(){
                $('#pager').FadeIn(1000);
            })
        });
        $(document).ready(function(){
            $('.file-uploads').click(function(){
                $('.files').slideToggle('slow');
            });
            $('.file-uploads').css('cursor','pointer');
            $('#close').css('cursor','pointer');
            $('#close2').css('cursor','pointer');
            $('.addProject').css('cursor','pointer');
            $('#close').click(function(){
                $('.files').slideToggle('slow');
                
            });
            $('#close2').click(function(){
                $('.files-2').slideToggle('slow');
            });
            $('#show-project').click(function(){
                $('.files-2').slideToggle('slow');
            });
            // $('body').click(function(){
            //     $('.files-2').show('slow');
            // });
            //File Increment
            $(".add-c-file").click(function(){ 
                var html = $(".clone").html();
                $(".increment").after(html);

            });
            //Remove form
            $("body").on("click",".remove-c-file",function(){ 
                $(this).parents(".control-group").remove();
            });
            //
            $('.addProject').click(function(){
                var ht = $(".project-increment").html();
                $(".pro-inc").after(ht);
            });
            $("#pro-done").click(function(){
                $('.files-2').slideToggle('slow');
            });
            $('.add-p-file').click(function(){
                // $('.p-file').toggle('slow');
                var htm = $(".p-file").html();
                $(".inc").after(htm);
            });
            $("body").on("click",".remove-p-file",function(){ 
                $(this).parents(".cont-group").remove();
            });        
        });
        new Vue({
            el: '#pro',
            data: {
                counter: 1
            },
            methods: {
                increase: function(){
                    this.counter++;
                }
            }
        });
    </script>
</html>
