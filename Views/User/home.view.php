<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Company Dir</title>
    <!-- Stylesheet files -->
    <link href="/Public/assets/css/animate.css" rel="stylesheet" />
    <link href="/Public/assets/css/bootstrap.min.css" rel="stylesheet" />
    <link href="/Public/css/main.css" rel="stylesheet" />
    <link href="http://netdna.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.css" rel="stylesheet">
    	<!--   Core JS Files   -->
	<script src="/Public/assets/js/jquery-2.2.4.min.js" type="text/javascript"></script>
    <script src="/Public/assets/js/bootstrap.min.js" type="text/javascript"></script>
    <script src="/Public/lib/typed.js" type="text/javascript"></script>
    <script defer src="https://use.fontawesome.com/releases/v5.3.1/js/all.js"></script>
    <style>
       
    </style>
</head>
<body>
    <div class="nav">
        <nav id="navbar" class="navbar navbar-default navbar-fixed-top" style="border-radius:0px;">
        <div class="container-fluid">
            <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>                        
            </button>
            <a class="navbar-brand" href="#">CompanyDir <i class="fas fa-sign-in-alt"></i></a>
            </div>
            <div class="collapse navbar-collapse" id="myNavbar">
            <ul class="nav navbar-nav">
                <li class="active"><a href="#">Home</a></li>
                <li class="dropdown">
                <a class="dropdown-toggle" data-toggle="dropdown" href="#">Create <span class="caret"></span></a>
                <ul class="dropdown-menu">
                    <li><a href="/auth/company/create">Add Company Account</a></li>
                    <li><a href="/auth/project/create">Add Project</a></li>
                </ul>
                </li>
                <li><a href="#second-section">Companies</a></li>
                <li><a href="#">Projects</a></li>
            </ul>
            <ul class="nav navbar-nav">
              <li><div class="form-group">
                <input type="text" class="search-input" id="typed4"><input type="submit" class="search_submit_button">
              </div>

              </li>
              
            </ul>
            <ul class="nav navbar-nav navbar-right">
                
                <!-- <li><a href="#"><span class="glyphicon glyphicon-log-in"></span> Login</a></li> -->
                <li class="dropdown">
                  <?php  if(isset($_SESSION['user_name'])):?>
                      <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                        <i class="fas fa-user-circle"></i>   <?php  echo $_SESSION['user_name'];?> <span class="caret"></span>
                      </a>
                  <?php endif;?>
                  <ul class="dropdown-menu">
                      <li id='user_info'><a href="javascript:void()"> <i class="fas fa-address-card"></i> Profile</a></li>
                      <li><a href="#"><i class="fas fa-user-cog"></i> Settings</a></li>
                      <li><a href="/auth/logout"><i class="fas fa-sign-out-alt"></i> Logout</a></li>
                  </ul>
                </li>
            </ul>
            </div>
        </div>
        </nav>
        <div class="header">
          <div class="progress-container">
            <div class="progress-bar" id="myBar"></div>
          </div>  
        </div>
    </div>
    <!-- Carousel starts -->
    <div id="sliderStart">   
      <div class="carousel-container">
        <div class="carousel">
          <div id="item1" class="carousel-item">
            <div>
              <p class="p1">
            
              </p>
              <p>
              <i class="fas fa-chart-bar"></i>
              </p>
            </div>
          </div>
          <div id="item2" class="carousel-item">
            <div>
              <p class="p2">
            
              </p>
              <p>
              <i class="fas fa-external-link-alt"></i>
              </p>
            </div>
          </div>
          <div id="item3" class="carousel-item">
            <div>
              <p class='p3'>
            
              </p>
              <p>
              <i class="fas fa-comments-dollar"></i>
              </p>
            </div>
          </div>
          <div id="item4" class="carousel-item">
          <div>
              <p class="p4">
            
              </p>
              <p>
              <i class="fas fa-globe"></i>
              </p>
            </div>
          </div>
        </div>
        <div class="carousel-controls">
          <a href="#" class="left" onclick="showSlide(true); return false;"><i class="fa fa-chevron-circle-left"></i> </a>
          <a href="#" class="right" onclick="showSlide(false); return false;"> <i class="fa fa-chevron-circle-right"></i></a>
        </div>

      </div>
    </div>
    <!-- carousel ends -->
    <!-- Divided Text -->
    <div class="centered-text">
      <!-- <h1><i class="far fa-calendar-plus"></i></h1> -->
      <h1></h1>
    </div>
    <!-- Division Text Ended -->
    
    <!-- Profile card -->
    <div class="" id="profile" style="display:none;border-radius:3px;background:white;position:absolute;top:0px;bottom:0px;right:0px;left:0px;margin:100px;height:80%;color:white;width:30%;margin:auto;margin-top:90px;">
      <div class="img" style="width:100%;height:230px;background:black;text-align:center;opacity:0.8;">
      <span style="" id="close_user_info"><i class="fa fa-times"></i></span>
        <?php if(isset($_SESSION['user_name'])):?>
          <?php  if($_SESSION['user_gender'] == 'Female'):?>
          <img src="../Public/assets/img/user-f-1.png" alt="" style="width:250px;">
          <?php else:?> 
          <img src="../Public/assets/img/user-m-1.png" alt="" style="width:250px;">
          <?php endif; ?>
        <?php endif; ?>
      </div>
      <div id="u-info">
      <?php if(isset($_SESSION['user_name'])):?>
        <h2 class="h2 user_name"><?php echo $_SESSION['user_name']; ?></h2>
        <h4>Phone:</br><?php echo $_SESSION['user_phone']; ?><span class="edit"><i class="far fa-edit"></i></span></h4>
        <h4>Email:</br><?php echo $_SESSION['user_email_address']; ?> <span class="edit"><i class="far fa-edit"></i></span></h4>
        <h4>Location:</br><?php echo $_SESSION['user_location']; ?> <span class="edit"><i class="far fa-edit"></i></span></h4>
        
        <?php endif; ?>
      </div>
    </div>
    <!-- Profile card Ends -->
    <div id="second-section">
      <div class="company_lists">
        <div class="container">
          <div class="row">
            
              <?php if(isset($companies) && !empty($companies)): ?>
              <?php foreach($companies as $company): ?>
                <div class="col-md-4">
                  <div class="box animated slideInDown">
                    <div class="top-show">
                      <img class="logo" src="/Public/assets/img/c_logo.png" alt="">
                      <h4><?php echo $company->company_name; ?></h4>
                    </div>
                    <hr>
                      <h6>Owner: <?php echo $company->created_by; ?></h6>
                      <h6>Email: <?php echo $company->email_address; ?></h6>
                      <h6>Tel: <?php echo $company->phone_number; ?></h6>
                      <h6>Location: <?php echo $company->location; ?></h6>
                        <h4 style="padding:10px">Completed Projects</h4>
                        <button style="margin-top:-40px;float:right;" class="btn btn-sm btn-success">Show</button>
                      <ul>
                        <li>OSB PROJET</li>
                        <li>OSB PROJET</li>
                        <li>OSB PROJET</li>
                      </ul>
                      <small>Existence since <?php echo $company->registered_on ?></small>
                  </div>
                </div>
              <?php endforeach; ?>
              <?php endif; ?>
            
          </div>
        </div>
      </div>
    </div>
    <!-- Companies project container -->
    <div class="" id="profile" style="display:block;border-radius:3px;padding:20px;background:white;position:absolute;top:0px;bottom:0px;right:0px;left:0px;margin:100px;margin:auto;margin-top:90px;margin:90px 150px 150px 150px;">
      <h4>Please select the company</h4>
      <ul>
        <li></li>
      </ul>
    </div>
   <!-- Message Box -->
    <?php view('Layouts/messageBox'); ?>
   <!-- Message Box ends -->
    <!-- scroll spy -->
    <div class="container" style="font-size:30px;position:fixed;top:0px;bottom:0px;right:0px;left:1100px;margin-top:600px;height:20px;color:white;width:20px;">
      
      <a id="arrow-down">
        <i class="fas fa-arrow-down"></i>
</a>
      <a id="arrow-up" style="display:none;" href="#sliderStart">
      <i class="fas fa-arrow-up"></i>
</a>
    </div>
    <!-- Footer -->
    <div class="footer">
      <h4>All Rights Reserved</h4>
    </div>
    <!-- Js codes -->
    <script src="../Public/assets/js/main.js"></script>
    <script>
     
    </script>
</body>
</html>