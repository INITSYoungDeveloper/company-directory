<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <link rel="stylesheet" href="../Public/assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="../Public/assets/css/bulma.css">
    <link href="https://fonts.googleapis.com/css?family=Noto+Sans+HK&display=swap" rel="stylesheet">
    <script defer src="https://use.fontawesome.com/releases/v5.3.1/js/all.js"></script>
    <style>
@import 'https://fonts.googleapis.com/css?family=Lato&display=swap';
body{
    background: linear-gradient(rgba(30, 34, 30, 0.377),rgba(8, 8, 8, 0.377)),url(../Public/assets/img/wizard-city.jpg);
    background-size: cover;
    background-repeat: no-repeat; 
    height: 920px;
    width:100%;
    overflow: hidden;
    font-family: ‘Nunito’, Montserrat, lato !important;
}
body::-webkit-scrollbar{
    display:none;
}
*{
    font-family: Arial, Helvetica, sans-serif;
}
.container{
    margin:auto;
    text-align:center;
    margin-top:20px;
    background:rgba(255,255,255,0.2);
    width:30%;
    padding:20px;
}
h1{
    font-size: 40px;
    font-weight:lighter;
    padding:30px; 
    color:rgba(75, 75, 75, 0.966);
    text-shadow:2px 1px 7px rgba(99, 93, 93, 0.507);
}
.input{
    margin:20px 0px 0px 0px;
    padding:12px 20px;
    background: rgba(30, 34, 30, 0.377);
    color:white;
    border:none;
    border-radius:3px;
    font-size:20px;

}
.form-group:hover .color{
    width:280px;
    transition:.4s;
    background: rgb(2, 2, 34);
}
.color{
    background:white;
    width:0px;
    height: 5px;
    margin:auto;
    transition: .4s;
}
.input::placeholder{
    color:silver;
    font-size:19px;
}
select option{
    font-size:14px;
}
.form-group{
    width:280px;
    margin:auto;
}
.submit{
    padding:10px 20px;
    border:none;
    width:280px;
    background: rgb(2, 2, 34);
    color: white;
    font-size:20px;
    border-radius:3px;
    margin-top:20px;
    transition:.5s;
}
.submit:hover{
    background:white;
    color:rgb(2, 2, 34);
    transition:.5s;
}
p{
    color:white;
}
p a{
    color:rgb(2, 2, 34);
}

    </style>
</head>
<body>
<div class="container">
        <h1>Sign up</h1>
        <?php view('Layouts/validation'); ?>
        <div class="form">
            <form action="/auth/register" method="POST">
                <div class="form-group">
                    <input type="text" class="input" placeholder="**Fullname**" name="fullname" required>
                    <div class="color"></div>
                </div>
                <div class="form-group">
                    <select class="form-control input" id="sel1" name="gender">
                        <option style="color:silver;">**Gender**</option>
                        <option value='Male'>Male</option>
                        <option value="Female">Female</option>
                    </select>
                </div>
                <div class="form-group">
                    <input type="tel" placeholder="**Phone Number**" class="input" name="phone" required>
                    <div class="color"></div>
                </div>
                <div class="form-group">
                    <select class="form-control input" id="sel1" name="field">
                        <option style="color:silver;">**Field**</option>
                        <option value='Developer'>Developer</option>
                        <option value="Designer">Designer</option>
                        <option value="Business Administrator">Business Administrator</option>
                        <option value="Software Engineer">Software Engineer</option>
                        <option value="Electrical Engineer">Electrical Engineer</option>
                    </select>
                </div>
                <div class="form-group">
                    <input type="text" placeholder="**Location**" class="input" name="location" required>
                    <div class="color"></div>
                </div>
                <div class="form-group">
                    <input type="text" placeholder="**Email Address**" class="input" name="email" required>
                    <div class="color"></div>
                </div>
                
                <div class="form-group">
                    <input type="password" placeholder="**Password**" class="input" name="password" required>
                    <div class="color"></div>
                </div>
                <div class="form-group">
                    <input type="password" placeholder="**Confirm Password**" class="input" name="confirm_password" required>
                    <div class="color"></div>
                </div>
                <div class="form-group">
                    <input type="submit" class="submit" value="Sign up">
                    <div class="color"></div>
                </div>
            </form>
            <hr>
            <p>Already have an account? <a href="/auth/login">Login</a></p>
        </div>
    </div>

    <!-- Core JS FILES -->
    <script src="/Public/assets/js/jquery-2.2.4.min.js" type="text/javascript"></script>
    <script src="/Public/assets/js/bootstrap.min.js" type="text/javascript"></script>
</body>
</html>