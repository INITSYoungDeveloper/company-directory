<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <link rel="stylesheet" href="../Public/assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="../Public/assets/css/bulma.css">
    <link href="https://fonts.googleapis.com/css?family=Noto+Sans+HK&display=swap" rel="stylesheet">
    <script defer src="https://use.fontawesome.com/releases/v5.3.1/js/all.js"></script>
    <style>
        @import 'https://fonts.googleapis.com/css?family=Lato&display=swap';
        body{
            background: linear-gradient(rgba(30, 34, 30, 0.377),rgba(8, 8, 8, 0.377)),url(../Public/assets/img/wizard-city.jpg);
            background-size: cover;
            background-repeat: no-repeat; 
            height: 800px;
            width:100%;
            overflow: hidden;
            font-family: ‘Nunito’, Montserrat, lato !important;
        }
        body::-webkit-scrollbar{
            display:none;
        }
        *{
            font-family: Arial, Helvetica, sans-serif;
        }
        .container{
            margin:auto;
            text-align:center;
            margin-top:50px;
            background:rgba(255,255,255,0.2);
            padding:20px;
            width:28%;
            border-radius:3px;
            /* border:1px solid palegreen; */
            
        }
        h1{
            font-size: 40px;
            font-weight:lighter;
            padding:30px; 
            color:rgba(75, 75, 75, 0.966);
            text-shadow:2px 1px 7px rgba(99, 93, 93, 0.507);
        }
        .input{
            margin:20px 0px 0px 0px;
            padding:12px 20px;
            background: rgba(30, 34, 30, 0.377);
            color:white;
            border:none;
            border-radius:3px;
            font-size:20px;

        }
        .form-group:hover .color{
            width:280px;
            transition:.4s;
            background: rgb(2, 2, 34);
        }
        .color{
            background:white;
            width:0px;
            height: 5px;
            margin:auto;
            transition: .4s;
        }
        .input::placeholder{
            color:silver;
            font-size:19px;
        }
        select option{
            font-size:14px;
        }
        .form-group{
            width:280px;
            margin:auto;
        }
        .submit{
            padding:10px 20px;
            border:none;
            width:280px;
            background: rgb(2, 2, 34);
            color: white;
            font-size:20px;
            border-radius:3px;
            margin-top:20px;
            transition:.5s;
        }
        .submit_google{
            background:rgba(82, 6, 6, 0.63);
        }
        .submit:hover{
            background:white;
            color:rgb(2, 2, 34);
            transition:.5s;
        }
        .box{
            width:50%;
            margin:auto;
        }
        p{
            color:white;
        }
        p a{
            color:rgb(2, 2, 34);
        }
    </style>
</head>
<body>
<div class="container">
        <h1>Sign in</h1>
        <?php view('Layouts/validation'); ?>
        <div class="form-group" style="display:flex;">
            <img src="../Public/assets/img/g-logo.png" alt="" style="width:50px;height:50px;position:absolute;margin-top:20px;border:2px solid white;">
            <form action="/auth/login/withgoogle" method="get">
                <input type="submit" class="submit submit_google" value="Sign in with google">
            </form>
        </div>
        
        <div class="form">
            <form action="/auth/login" method="POST">
                <div class="form-group">
                    <input type="text" placeholder="**Email Address**" class="input" name="email" required>
                    <div class="color"></div>
                </div>
                <div class="form-group">
                    <input type="password" placeholder="**Password**" class="input" name="password" required>
                    <div class="color"></div>
                </div>
                <div class="form-group">
                    <input type="submit" class="submit" value="Sign in">
                    <div class="color"></div>
                </div>
            </form>
            <hr>
            <p>Don't have account yet? <a href="/auth/register">Register</a></p>
        </div>
    </div>

    <!-- Core JS FILES -->
    <script src="/Public/assets/js/jquery-2.2.4.min.js" type="text/javascript"></script>
    <script src="/Public/assets/js/bootstrap.min.js" type="text/javascript"></script>
    <script>
        $(document).ready(function(){
            setTimeout(function(){
                $('.error').slideUp(200);
                $('.success').slideUp(200);
            },5000);
        });
    </script>
</body>
</html>