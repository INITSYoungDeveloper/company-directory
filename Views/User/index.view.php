
<?php 
    define('APP_TITLE','Home');
    view('layouts/app/head');
?>
<style>
    .cont {
  position: relative;
  width: 100%;
}

.image {
  display: block;
  width: 100%;
  height: auto;
}

.overlay {
  position: absolute;
  bottom: 0;
  left: 20;
  right: 0;
  background-color: #4E73DF;
  cursor: pointer;
  overflow: hidden;
  width: 0;
  height: 100%;
  transition: .5s ease;
}

.cont:hover .overlay {
  width: 100%;
}

.text {
  color: white;
  font-size: 14px;
  position: absolute;
  top: 50%;
  left: 50%;
  margin-left: 10px;
  text-align: center;
  width: 100%;
  -webkit-transform: translate(-50%, -50%);
  -ms-transform: translate(-50%, -50%);
  transform: translate(-50%, -50%);
  white-space: nowrap;
  overflow: auto;
  height: 100px;
  overflow-x: hidden;
}
.text::-webkit-scrollbar{
    background: white;
    opacity: 0.3;
    border-radius: 50px;
}
.text::-webkit-scrollbar-thumb {
    border-radius: 10px;
    -webkit-box-shadow: inset 0 0 6px rgba(0,0,0,0.5); 
}
.text li{
    padding: 10px;
    list-style: square;
    opacity: 0.5;
}
</style>
<!-- Page Heading -->
<h6 class="h6 mb-4 text-gray-800">Home /</h6>
<div class="row">
    <!-- Companies Card Example -->
    <div class="col-xl-3 col-md-6 mb-4">
        <div class="card border-left-primary shadow h-100 py-2 animated slideInRight">
        <div class="card-body">
            <div class="row no-gutters align-items-center">
            <div class="col mr-2">
                <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">Companies</div>
                <div class="h5 mb-0 font-weight-bold text-gray-800">
                    <?php echo count($authUserCompanies); ?>
                </div>
            </div>
            <div class="col-auto">
                <i class="fas fa-calendar fa-2x text-gray-300"></i>
            </div>
            </div>
        </div>
        </div>
    </div>
    <!-- Projects Card Example -->
    <div class="col-xl-3 col-md-6 mb-4">
        <div class="card border-left-success shadow h-100 py-2 animated slideInLeft">
        <div class="card-body">
            <div class="row no-gutters align-items-center">
            <div class="col mr-2">
                <div class="text-xs font-weight-bold text-success text-uppercase mb-1">Projects</div>
                <div class="h5 mb-0 font-weight-bold text-gray-800">0</div>
            </div>
            <div class="col-auto">
                <i class="fas fa-project-diagram fa-2x text-gray-300"></i>
            </div>
            </div>
        </div>
        </div>
    </div>
    <!-- Earnings (Monthly) Card Example -->
    <div class="col-xl-3 col-md-6 mb-4">
        <div class="card border-left-info shadow h-100 py-2 animated slideInRight">
        <div class="card-body">
            <div class="row no-gutters align-items-center">
            <div class="col mr-2">
                <div class="text-xs font-weight-bold text-info text-uppercase mb-1">Tasks</div>
                <div class="row no-gutters align-items-center">
                <div class="col-auto">
                    <div class="h5 mb-0 mr-3 font-weight-bold text-gray-800">50%</div>
                </div>
                <div class="col">
                    <div class="progress progress-sm mr-2">
                    <div class="progress-bar bg-info" role="progressbar" style="width: 50%" aria-valuenow="90" aria-valuemin="0" aria-valuemax="100"></div>
                    </div>
                </div>
                </div>
            </div>
            <div class="col-auto">
                <i class="fas fa-clipboard-list fa-2x text-gray-300"></i>
            </div>
            </div>
        </div>
        </div>
    </div>

    <!-- Pending Requests Card Example -->
    <div class="col-xl-3 col-md-6 mb-4">
        <div class="card border-left-warning shadow h-100 py-2 animated slideInLeft">
        <div class="card-body">
            <div class="row no-gutters align-items-center">
            <div class="col mr-2">
                <div class="text-xs font-weight-bold text-warning text-uppercase mb-1">Company Categories</div>
                <div class="h5 mb-0 font-weight-bold text-gray-800">0</div>
            </div>
            <div class="col-auto">
                <i class="fas fa-layer-group fa-2x text-gray-300"></i>
            </div>
            </div>
        </div>
        </div>
    </div>
</div>
<!-- jhjh -->
<div class="row">
        <?php if(count($authUserCompanies) > 0): ?>
            <?php foreach($authUserCompanies as $company): ?>
            <div class="col-md-6">
                <div class="card shadow mb-4">
                    <!-- Card Header - Accordion -->
                    <a href="/companies/dashboard?no=<?php echo $company->id; ?>" class="d-block card-header py-3" data-toggle="" role="button" aria-expanded="true" aria-controls="collapseCardExample">
                        <h6 class="m-0 font-weight-bold text-primary"><?php echo $company->name; ?> <span style="float:right;"><i class="fas fa-caret-right"></i></span></h6>
                    </a>
                    <!-- <a style="border-radius:0px;border-top-right-radius: 10px;border-top-left-radius: 10px" class="btn btn-sm btn-primary text-light" href="/companies/dashboard?no=<?php echo $company->id; ?>">Visit Dashboard</a> -->
                    <!-- Card Content - Collapse -->
                    <div class="collapse show " id="collapseCardExample">
                        <div class="card-body cont">
                            <div class="image">
                                <?php echo $company->about; ?>
                                
                            </div>
                            <div class="overlay">
                                <div class="text row">
                                    
                                    <?php
                                        $query = require 'core/bootstrap.php';
                                        $categories = $query->selectWhere('categories','company_id',$company->id);
                                        if(count($categories) > 0):
                                        foreach($categories as $category):
                                    ?>
                                        <li>  <?php echo $category->name; ?></li>
                                        
                                        <?php endforeach; ?>
                                    <?php else: ?>
                                            <br><br>
                                       <li style="margin-top:20px;">No categories is created yet!</li> 
                                    <?php endif; ?>
                                </div>
                            </div>
                        </div>
                       
                    </div>
                    </div>
            </div>
            
        <?php endforeach; ?>
        <?php else:  ?>
                <div class="col-md-6">
                    <div class="card shadow mb-4">
                    <!-- Card Header - Accordion -->
                    <a href="#collapseCardExample" class="d-block card-header py-3" data-toggle="collapse" role="button" aria-expanded="true" aria-controls="collapseCardExample">
                        <h6 class="m-0 font-weight-bold text-primary">Notice!!</h6>
                    </a>
                    <!-- Card Content - Collapse -->
                    <div class="collapse show" id="collapseCardExample">
                        <div class="card-body">
                        No company account is created yet!
                        </div>
                    </div>
                    </div>
                </div>
                <div class="col-md-6"> 
                    <div class="card shadow mb-4">
                    <!-- Card Header - Dropdown -->
                    <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                        <h6 class="m-0 font-weight-bold text-primary">categories</h6>
                        <div class="dropdown no-arrow">
                        <a class="dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <i class="fas fa-ellipsis-v fa-sm fa-fw text-gray-400"></i>
                        </a>
                        <div class="dropdown-menu dropdown-menu-right shadow animated--fade-in" aria-labelledby="dropdownMenuLink">
                            <div class="dropdown-header">Dropdown Header:</div>
                            <a class="dropdown-item" id="add-uncompany-category" href="javascript:void()">Add category</a>
                            
                            <div class="dropdown-divider"></div>
                            <a class="dropdown-item" href="#">Help</a>
                        </div>
                        </div>
                    </div>
                    <!-- Card Body -->
                    <div class="card-body">
                        No categories is created
                    </div>
                    </div>
                </div>
        <?php  endif;?>
    </div>
<!-- ksdjs -->

<?php  view('layouts/app/bottom');?>
<script>
    $('#add-uncompany-category').click(function(){
        $('#pageloadcontent').toggle('slow');
        $('#pageloadcontent .card-body').html('You will need to create a company account before you can create any category. Thanks!');
    });
</script>