
<?php
    define('APP_TITLE','Lost Password');
    require 'core/auth.php';
    view('layouts/auth/head'); 
    session_start();
?>
    <!-- Outer Row -->
    <div class="row justify-content-center">

      <div class="col-xl-10 col-lg-12 col-md-9">

        <div class="card o-hidden border-0 shadow-lg my-5">
          <div class="card-body p-0">
            <div class="row">
              <div class="col-lg-6 d-none d-lg-block bg-lost-password-image">
              </div>
              <div class="col-lg-6">
                <div class="p-5">
                  <div class="text-center">
                    <h1 class="h4 text-secondary has-text-weight-bold  mb-4">Forgot Password!</h1>
                  </div>
                  <form class="user" method="POST" action="/auth/lost_password">
                    <div class="form-group">
                       <input id="lost_email exampleInputEmail" type="email" class="form-control form-control-user" name="email" required autocomplete="email" autofocus aria-describedby="emailHelp" placeholder="Enter Email Address...">
                        <div style="font-size:12.8px;" id="email_error"></div>
                    </div>
                    <div class="form-group">
                      <input type="password" class="form-control form-control-user" id="exampleInputPassword lost_password" placeholder="New Password" name="password" required autocomplete="current-password">
                    <div style="font-size:12.8px;" id="password_error"></div>
                    </div>
                    <div class="form-group">
                      <input type="password" class="form-control form-control-user" id="exampleInputPassword lost_confirm_password" placeholder="Confirm Password" name="confirm_password" required autocomplete="current-password">
                    <!-- <div style="font-size:12.8px;" id="password_alertmsg"></div> -->
                    </div>
                    <div class="form-group">
                      <div class="custom-control custom-checkbox small">
                        <input type="checkbox" class="custom-control-input" id="customCheck">
                        <label class="custom-control-label" for="customCheck">Remember Me</label>
                      </div>
                    </div>
                    <input type="submit" class="btn btn-primary btn-user btn-block" value="Sign in">
                    <hr>
                    <a href="<?php echo $url ?>" class="btn btn-google btn-user btn-block">
                      <i class="fab fa-google fa-fw"></i> Sign in with Google
                    </a>
                    <!-- <a href="index.html" class="btn btn-facebook btn-user btn-block">
                      <i class="fab fa-facebook-f fa-fw"></i> Login with Facebook
                    </a> -->
                  </form>
                  <hr>
                  <div class="text-center">
                    <a class="small" href="/auth/login">Sign in</a>
                  </div>
                  <div class="text-center">
                    <a class="small" href="/auth/register">Create an Account!</a>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>

      </div>

    </div>

  </div>
<?php view('layouts/auth/bottom'); ?>
<script>



</script>