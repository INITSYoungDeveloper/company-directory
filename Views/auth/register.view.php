
<?php
    define('APP_TITLE','Register');
    // global $title;
    // $title = 'Register';
    view('layouts/auth/head'); 
    // session_start();
?>

<div class="card o-hidden border-0 shadow-lg my-5">
      <div class="card-body p-0">
        <!-- Nested Row within Card Body -->
        <div class="row">
          <div class="col-lg-5 d-none d-lg-block bg-register-image"></div>
          <div class="col-lg-7">
            <div class="p-5">
              <div class="text-center">
                <h1 class="h4 text-gray-900 mb-4">Create an Account!</h1>
              </div>
              <form class="user" method="POST" id="formData" action="/auth/register">
                <div class="form-group row">
                  <div class="col-sm-6 mb-3 mb-sm-0">
                    <input type="text" name="firstname" class="form-control form-control-user" id="exampleFirstName" placeholder="First Name" required>
                    <div style="font-size:12.8px;" id="firstname_alertmsg"></div>
                  </div>
                  <div class="col-sm-6">
                    <input type="text" name="lastname" class="form-control form-control-user" id="exampleLastName" placeholder="Last Name" required>
                    <div style="font-size:12.8px;" id="lastname_alertmsg"></div>
                  </div>
                </div>
                <div class="form-group row">
                  <div class="col-sm-6 mb-3 mb-sm-0">
                    <input type="tel" class="form-control form-control-user" name="phone" id="examplePhone" placeholder="Contact" required>
                    <div style="font-size:12.8px;" id="phone_alertmsg"></div>
                  </div>
                  <div class="col-sm-6">
                    <select style="height: 48px;font-size:12.8px;color:#6E707E;font-weight: lighter"  id="exampleGender" class="form-control" name="gender">
                        <option value="Gender">Gender</option>
                        <option value="Male">Male</option>
                        <option value="Female">Female</option>
                    </select>
                     <div style="font-size:12.8px;" id="gender_alertmsg"></div>
                  </div>
                </div>
                <div class="form-group">
                  <input type="email" name="email" class="form-control form-control-user" id="exampleEmail" placeholder="Email Address" required>
                  <div style="font-size:12.8px;" id="email_alertmsg"></div>
                </div>
                <div class="form-group">
                  <input type="text" name="location" class="form-control form-control-user" id="exampleLocation" placeholder="Location" required>
                  <div style="font-size:12.8px;" id="location_alertmsg"></div>
                </div>
                <div class="form-group row">
                  <div class="col-sm-6 mb-3 mb-sm-0">
                    <input type="password" name="password" class="form-control form-control-user" id="examplePassword" placeholder="Password" required>
                    <div style="font-size:12.8px;" id="password_alertmsg"></div>
                  </div>
                  <div class="col-sm-6">
                    <input type="password" name="password_confirmation" class="form-control form-control-user" id="exampleRepeatPassword" placeholder="Repeat Password" required>
                  </div>
                </div>
                <button type="submit" class="btn btn-primary btn-user btn-block signUpBtn">
                   Sign up
                </button>
              </form>
              <hr>
              <div class="text-center">
                <a class="small" href="/auth/lost_password">Forgot Password?</a>
              </div>
              <div class="text-center">
                <a class="small" href="/auth/login">Already have an account? Login!</a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

<?php view('layouts/auth/bottom'); ?>
<script>
// Validate form before submit
    $('form#formData').submit(function(){
       if($('#exampleFirstName').val().length < 3){
           $('#exampleFirstName').addClass('is-invalid');

            $('#firstname_alertmsg').html("Your name is required Please!").css('color','#E74A3B').css('font-weight','bolder').fadeIn('slow');
            $('#firstname_alertmsg').addClass('animated bounceInDown');
            return false;
       }else if($('#exampleLastName').val().length < 3){
            $('#exampleLastName').addClass('is-invalid');
            $('#lastname_alertmsg').html("Your lastname is required Please!").css('color','#E74A3B').css('font-weight','bolder').fadeIn('slow');
            $('#lastname_alertmsg').addClass('animated bounceInDown');
            return false;
       }else if($('#examplePhone').val().length < 3){
            $('#examplePhone').addClass('is-invalid');
            $('#phone_alertmsg').html("Your lastname is required Please!").css('color','#E74A3B').css('font-weight','bolder').fadeIn('slow');
            $('#phone_alertmsg').addClass('animated bounceInDown');
            return false;
       }else if($('#exampleGender').val() == 'Gender'){
            $('#exampleGender').addClass('is-invalid');
            $('#gender_alertmsg').html("Your gender is required Please!").css('color','#E74A3B').css('font-weight','bolder').fadeIn('slow');
            return false;
       }else if($('#exampleLocation').val().length < 3){
            $('#exampleLocation').addClass('is-invalid');
            $('#location_alertmsg').html("Your location is required Please!").css('color','#E74A3B').css('font-weight','bolder').fadeIn('slow');
            return false;
       }else if($('#examplePassword').val().length < 3){
            $('#examplePassword').addClass('is-invalid');
            $('#password_alertmsg').html("Your password is required Please!").css('color','#E74A3B').css('font-weight','bolder').fadeIn('slow');
            return false;
       }else if($('#examplePassword').val() !== $('#exampleRepeatPassword').val()){
            $('#examplePassword').addClass('is-invalid');
            $('#exampleRepeatPassword').addClass('is-invalid');
            $('#password_alertmsg').html("Your passwords doesn't match").css('color','#E74A3B').css('font-weight','bolder').fadeIn('slow');
            return false;
       }else{
           return true;
       }
    });
</script>