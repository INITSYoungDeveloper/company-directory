<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title><?php echo APP_TITLE ?></title>

  <!-- Custom fonts for this template-->
  <link href="/Public/app/vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
  <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">
    <link href="/Public/css/app.css" rel="stylesheet">
  <!-- Custom styles for this template-->
  <link href="/Public/assets/css/animate.css" rel="stylesheet">
  <link href="/Public/app/css.1/sb-admin-2.min.css" rel="stylesheet">
<script src="/Public/js/app.js"></script>
<style>
    
.create-company-overlay {
  position: absolute;
  background: rgba(0, 0, 0, 0.664);
  height: 657px;
  width: 100%;
  font-size: 14px;
  padding: 0 68px 0 68px;
  top: 0;
  bottom: 0;
  right: 0;
  left: 0;
  color: white;
  text-align: right;
}
.create-company-overlay h4.closebtn {
  margin: 40px;
  cursor: pointer;
}
.create-company-overlay .box {
  text-align: left;
  background-color: white;
  border-radius: 6px;
  box-shadow: 0 2px 3px rgba(10, 10, 10, 0.1), 0 0 0 1px rgba(10, 10, 10, 0.1);
  color: #4a4a4a;
  display: block;
  margin: auto;
  width: 652px;
  top: 50%;
  left: 50%;
  height: 400px;
  box-shadow: 4px 4px 40px black;
}
.create-company-overlay .box .errors {
  color: #970a0a;
}
.create-company-overlay .box .box-bottom {
  margin-top: -20px;
  text-align: right;
}
.create-company-overlay .box .box-bottom h3.btn {
  padding: 2px 10px;
  margin-top: 5px;
  margin-right: 50px;
}
.create-company-overlay .box .box-bottom .file_upload {
  position: absolute;
  margin-top: -37px;
  width: 40px;
  opacity: 0;
  cursor: pointer !important;
  margin-left: -77px;
}
.create-company-overlay .box .box-bottom button {
  margin-left: 20px;
  font-weight: bolder;
  border: none;
}
.create-company-overlay .box .box-bottom a {
  text-decoration: none;
  color: #3490DC;
  font-weight: bolder;
  border: none;
}
.create-company-overlay .box .box-bottom :hover {
  color: white;
}
.create-company-overlay .box .form {
  overflow-y: auto;
  overflow-x: hidden;
  height: 300px;
  scroll-behavior: smooth;
  font-size: 14px;
}
.create-company-overlay .box .header {
  border-bottom: 1px solid silver;
  padding: 1.15rem;
  padding-bottom: 0.5rem;
  font-weight: bolder;
}
.create-company-overlay .box .header h4 {
  font-weight: bold;
  color: silver;
  font-size: 16px;
}
.create-company-overlay .box .row {
  padding: 1.25rem;
}
.create-company-overlay .box .comp-name {
  text-align: center;
  font-size: 15px;
  color: silver;
  margin-top: 13px;
}
.create-company-overlay .warning {
  box-shadow: none;
  padding: 1.25rem;
  color: #800000;
}
.create-company-overlay .warning p{
    color: silver;
    font-weight: bolder;
}
.create-company-overlay .success {
  background: lightgreen;
  padding: 1.25rem;
  box-shadow: none;
  color: green;
}

</style>

</head>

<body class="bg-gradient-primary">

  <div class="container animated fadeInDown">