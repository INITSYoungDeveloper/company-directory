</div>
    <!-- Message box -->
    <?php  
    $error = session('error');
    $success = session('success');
    if(isset($success) OR isset($error)): ?>
        <div class="create-company-overlay message-box" style="display:block">
            <br><br><br><br>
            <?php  if (isset($error)): ?>
            <div class="box warning animated bounceInRight border-left-danger" style="margin-top: 40px;height: 200px">
                <div class="row" style="padding:0px">
                   <div class="col-md-6">
                       <h1>Warning!</h1>    
                   </div>
                   <div class="col-md-6" style="text-align: right;">
                        <button class="btn btn-danger alertbtn" style="padding:2px 10px">Ok</button>
                   </div>
                </div>
                <hr>
                <p  class="text-danger" style="font-weight:normal;"><?php echo session('error'); ?></p>
            </div>
            <?php  endif;?>
            <?php  if (isset($success)):?>
            <div class="box success animated bounceInRight border-left-success" style="margin-top: 40px;height: 200px;background:white;">
                <div class="row " style="padding:0px">
                   <div class="col-md-6">
                       <h1>Success!</h1>
                   </div>
                   <div class="col-md-6" style="text-align: right;">
                        <button class="btn btn-success alertbtn" style="padding:2px 10px">Ok</button>
                   </div>
                </div>
                <hr>
                <p><?php  echo session('success'); ?></p>
                
            </div>
            <?php  endif;?>
        </div>
    <?php  endif; ?>
  <!-- Bootstrap core JavaScript-->
  <script src="/Public/vendor/jquery/jquery.min.js"></script>
  <script src="/Public/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

  <!-- Core plugin JavaScript-->
  <script src="/Public/vendor/jquery-easing/jquery.easing.min.js"></script>
    
  <!-- Custom scripts for all pages-->
  <script src="/Public/js.1/sb-admin-2.min.js"></script>
    <script>

        $('.alertbtn').click(function(){
            $('.message-box').slideToggle(200);
        });
    </script>
</body>

</html>
