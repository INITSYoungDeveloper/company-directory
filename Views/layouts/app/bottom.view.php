<!-- All forms loads here -->
            <div id="pageloadcontent" class="row " style="display: none;position:absolute;top:100px;left:500px;right:0px;bottom:0px;">
            <div class="col-md-6 animated bounceInDown">
              <div class="card shadow mb-4 border-left-info">
                <div class="card-header py-3">
                  <h6 class="m-0 font-weight-bold text-primary">Message!
                  <button class="btn btn-sm btn-right add-company-btn-close" style="float:right">Close</button>
                  </h6> 
                </div>
                <div class="card-body">
                  The styling for this basic card example is created by using default Bootstrap utility classes. By using utility classes, the style of the card component can be easily modified with no need for any custom CSS!
                </div>
              </div>
            </div>
            </div>
          </div>
          <div class="search-content">

          </div>
        </div>
        <!-- /.container-fluid -->

      </div>
      <!-- End of Main Content -->
      <!-- Footer -->
      <footer class="sticky-footer bg-white">
        <div class="container my-auto">
          <div class="copyright text-center my-auto">
            <span><i> Copyright &copy; INITS LIMITED 2019 </i> </span>
          </div>
        </div>
      </footer>
      <!-- End of Footer -->

    </div>
    <!-- End of Content Wrapper -->

  </div>
  <!-- End of Page Wrapper -->

  <!-- Scroll to Top Button-->
  <a class="scroll-to-top rounded" href="#page-top">
    <i class="fas fa-angle-up"></i>
  </a>

  <!-- Logout Modal-->
  <div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Ready to Leave?</h5>
          <button class="close" type="button" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
          </button>
        </div>
        <div class="modal-body">Select "Logout" below if you are ready to end your current session.</div>
        <div class="modal-footer">
          <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
          <a class="btn btn-primary" href="/auth/logout" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">Logout</a>
            <form id="logout-form" action="/auth/logout" method="POST" style="display: none;">
                
            </form>
        </div>
      </div>
    </div>
  </div>
<!-- Create company modal -->
  <div class="create-company-overlay create-company-form animated--grow-in" style="display: none">
        <h4 class="btn closebtn companyclosebtn" data-toggle="tooltip" title="Close" data-placement="bottom"><i class="fa fa-times-circle" aria-hidden="true"></i></h4>
        <div class="box">
        <form method="POST" action="/companies/create" id="createCompanyForm">
            <div class="header">
                <h4>Create new company</h4>
            </div>
            <div class="form">

     
                <div class="row">
                    <div class="col-md-1">
                        <div class="comp-name">
                            <i class="far fa-newspaper"></i>
                        </div>
                    </div>
                    <div class="col-md-5">
                        <label for="inp" class="inp">
                            <input type="text" autocomplete="off" id="inp" class="company_name" required name="company_name" placeholder="&nbsp;">
                            <span class="label">Company Name</span>
                            <span class="border"></span>
                        </label>
                        <div style="font-size:12.8px;" id="company_name_alertmsg"></div>
                    </div>
                    <div class="col-md-5">
                        <label for="inp" class="inp">
                            <input type="text" autocomplete="off" id="inp" class="company_manager_name" required name="company_manager_name" placeholder="&nbsp;">
                            <span class="label">Managing Director's Name</span>
                            <span class="border"></span>
                        </label>
                        <div style="font-size:12.8px;" id="company_manager_name_alertmsg"></div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-1">
                        <div class="comp-name">
                            <i class="fa fa-location-arrow" aria-hidden="true"></i>
                        </div>
                    </div>
                    <div class="col-md-5">
                        <label for="inp" class="inp">
                            <input type="text" autocomplete="off" id="inp" class="company_location" required name="company_location" placeholder="&nbsp;">
                            <span class="label">Location</span>
                            <span class="border"></span>
                        </label>
                        <div style="font-size:12.8px;" id="company_location_alertmsg"></div>
                    </div>
                    <div class="col-md-5">
                        <label for="inp" class="inp">
                            <input type="text" autocomplete="off" required name="company_website" id="inp" class="company_website" placeholder="&nbsp;">
                            <span class="label">Website</span>
                            <span class="border"></span>
                        </label>
                        <div style="font-size:12.8px;" id="company_website_alertmsg"></div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-1">
                        <div class="comp-name">
                            <i class="far fa-envelope-open"></i>
                        </div>
                    </div>
                    <div class="col-md-10">
                        <label for="inp" class="inp">
                            <input type="email" autocomplete="off" required name="company_email" id="inp" class="company_email" placeholder="&nbsp;">
                            <span class="label">Email</span>
                            <span class="border"></span>
                        </label>
                        <div style="font-size:12.8px;" id="company_email_alertmsg"></div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-1">
                        <div class="comp-name">
                            <i class="fas fa-phone"></i>
                        </div>
                    </div>
                    <div class="col-md-10">
                        <label for="inp" class="inp">
                            <input type="text" autocomplete="off" required name="company_phone" id="inp" class="company_phone" placeholder="&nbsp;">
                            <span class="label">Phone</span>
                            <span class="border"></span>
                        </label>
                        <div style="font-size:12.8px;" id="company_phone_alertmsg"></div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-1">
                        <div class="comp-name">
                            <i class="fa fa-comment-dots" aria-hidden="true"></i>
                        </div>
                    </div>
                    <div class="col-md-10">
                        <label for="inp" class="inp">
                            <textarea id="inp" class="company_about" required name="company_about" placeholder="&nbsp;"></textarea>
                            <span class="label">About</span>
                            <span class="border"></span>
                        </label>
                        <div style="font-size:12.8px;" id="company_about_alertmsg"></div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-1">
                        <div class="comp-name">
                            <i class="fas fa-calendar-check"></i>
                        </div>
                    </div>
                    <div class="col-md-10">
                        <label for="inp" class="inp">
                            <input type="date" required id="inp" class="company_existence_date" name="company_existence_date" placeholder="&nbsp;">
                            <span class="label">Existed since</span>
                            <span class="border"></span>
                        </label>
                        <div style="font-size:12.8px;" id="company_existence_date_alertmsg"></div>
                    </div>
                </div>
            </div>
            <div class="row box-bottom">
                <div class="col-md-11">
                    <a href="javascript:void()" class="btn btn-sm btn-outline-primary cancelbtn companycancelbtn">Cancel</a>
                    <button class="btn btn-sm btn-outline-primary savebtn">Save</button>
                </div>
            </div>
            </form>
        </div>
        <div class="row">
          <div class="upload-loader" style="display:none">
            <img class="upload-loader-file"  src="../Public/assets/img/load.gif"> Please Wait
          </div>
        </div>
        
    </div>
    <!-- Create company modal -->
  <div class="create-company-overlay create-categories animated--grow-in" style="display: none">
        <h4 class="btn categoryclosebtn closebtn" data-toggle="tooltip" title="Close" data-placement="bottom"><i class="fa fa-times-circle" aria-hidden="true"></i></h4>
        <div class="box" style="height:300px;">
        <form method="POST" action="/companies/categories/create" id="createCategoriesForm">
            <div class="header">
                <h4>Create new category</h4>
            </div>
            <?php
                $id = $_GET['no'];
                $query = require 'core/bootstrap.php';
                $company = $query->selectWhere('companies','id',$id)[0];
            ?>
            <input type="hidden" name="company_id" value="<?php echo $company->id ?>">
            
            <div class="form">
     
                <div class="row">
                    <div class="col-md-1">
                        <div class="comp-name">
                            <i class="far fa-newspaper"></i>
                        </div>
                    </div>
                    <div class="col-md-10">
                        <label for="inp" class="inp">
                            <input type="text" autocomplete="off" id="inp" class="category_name" required name="category_name" placeholder="&nbsp;">
                            <span class="label">Category Name</span>
                            <span class="border"></span>
                        </label>
                        <div style="font-size:12.8px;" id="category_name_alertmsg"></div>
                    </div>
                    
                </div>
                
            </div>
            <div class="row box-bottom" style="margin-top:-130px;">
                <div class="col-md-11">
                    <a href="javascript:void()" class="btn btn-sm btn-outline-primary cancelbtn categorycancelbtn">Cancel</a>
                    <button class="btn btn-sm btn-outline-primary savebtn">Save</button>
                </div>
            </div>
            </form>
        </div>
        <div class="row">
          <div class="upload-loader" style="display:none">
            <img style="bottom:0px" class="upload-loader-file"  src="../Public/assets/img/load.gif"> Please Wait
          </div>
        </div>
    </div>
<!-- Create Task Form -->
<div class="create-company-overlay create-task animated--grow-in" style="display: none">
<h4 class="btn taskclosebtn closebtn" data-toggle="tooltip" title="Close" data-placement="bottom"><i class="fa fa-times-circle" aria-hidden="true"></i></h4>
        <div class="box" style="height:300px;">
        <form method="POST" action="/projects/issues/create" id="createTasksForm">
            <div class="header">
                <h4>Create New Task</h4>
            </div>
            <?php
                $project_id = $_GET['no'];
            ?>
            <input type="hidden" name="project_id" value="<?php echo $project_id ?>">
            
            <div class="form">
     
                <div class="row">
                    <div class="col-md-1">
                        <div class="comp-name">
                        <i class="fa fa-location-arrow" aria-hidden="true"></i>
                        </div>
                    </div>
                    <div class="col-md-10">
                        <label for="inp" class="inp">
                            <textarea style="height: 150px" id="inp" class="create_task_input" required name="issue" placeholder="&nbsp;"></textarea>
                            <span class="label">Please tell what's all about...</span>
                            <span class="border"></span>
                        </label>
                        <div style="font-size:12.8px;" id="create_task_error"></div>
                    </div>
                    
                </div>
                
            </div>
            <div class="row box-bottom" style="margin-top:-130px;">
                <div class="col-md-11">
                    <a href="javascript:void()" class="btn btn-sm btn-outline-primary cancelbtn taskcancelbtn">Cancel</a>
                    <button class="btn btn-sm btn-outline-primary savebtn">Done</button>
                </div>
            </div>
            </form>
        </div>
        <div class="row">
          <div class="upload-loader" style="display:none">
            <img style="bottom:0px" class="upload-loader-file"  src="../Public/assets/img/load.gif"> Please Wait
          </div>
        </div>
    </div>

<!-- Create Task form ends -->
    <!-- Message box -->
    <?php  
    $error = session('error');
    $success = session('success');
    if(isset($success) OR isset($error)): ?>
        <div class="create-company-overlay message-box" style="display:block">
            <br><br><br><br>
            <?php  if (isset($error)): ?>
            <div class="box warning animated bounceInRight border-left-danger" style="margin-top: 40px;height: 200px">
                <div class="row" style="padding:0px">
                   <div class="col-md-6">
                       <h1>Warning!</h1>    
                   </div>
                   <div class="col-md-6" style="text-align: right;">
                        <button class="btn btn-danger alertbtn" style="padding:2px 10px">Ok</button>
                   </div>
                </div>
                <hr>
                <p  class="text-danger" style="font-weight:normal;"><?php echo session('error'); ?></p>
            </div>
            <?php  endif;?>
            <?php  if (isset($success)):?>
            <div class="box success animated bounceInRight border-left-success" style="margin-top: 40px;height: 200px;background:white;">
                <div class="row " style="padding:0px">
                   <div class="col-md-6">
                       <h1>Success!</h1>
                   </div>
                   <div class="col-md-6" style="text-align: right;">
                        <button class="btn btn-success alertbtn" style="padding:2px 10px">Ok</button>
                   </div>
                </div>
                <hr>
                <p><?php  echo session('success'); ?></p>
                
            </div>
            <?php  endif;?>
        </div>
    <?php  endif; ?>
        
  <!-- Bootstrap core JavaScript-->
  
  <!-- <script src="/Public/assets/js/jquery-ui.js"></script> -->
  <script src="/Public/app/vendor/jquery/jquery.min.js"></script>
  <script src="/Public/assets/js/jquery-ui.min.js"></script>
  <script src="/Public/app/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

  <!-- Core plugin JavaScript-->
  <script src="/Public/app/vendor/jquery-easing/jquery.easing.min.js"></script>

  <!-- Custom scripts for all pages-->
  <script src="/Public/app/js.1/sb-admin-2.min.js"></script>

    <!-- Page level plugins -->
    <script src="/Public/app/vendor/chart.js/Chart.min.js"></script>

    <!-- Page level custom scripts -->
    <script src="/Public/app/js.1/demo/chart-area-demo.js"></script>
    <script src="/Public/app/js.1/demo/chart-pie-demo.js"></script>
<!-- Table scripts -->
    <script src="/Public/app/vendor/datatables/jquery.dataTables.min.js"></script>
    <script src="/Public/app/vendor/datatables/dataTables.bootstrap4.min.js"></script>

    <!-- Page level custom scripts -->
    <script src="/Public/app/js.1/demo/datatables-demo.js"></script>

  <script>
    $('.companycancelbtn').click(function(){
        $('.create-company-form').slideToggle('slow');
    });
    $('.categorycancelbtn').click(function(){
        $('.create-categories').slideToggle('slow');
    });
    $('.alertbtn').click(function(){
        $('.message-box').toggle('slow');
    });
    $('.add-company-btn-close').click(function(){
        $('#pageloadcontent').slideToggle();
    });
    // 
    // create categories form 
    $('form#createCategoriesForm').on('submit', function(){
        if($('.category_name').val().length < 3){
            $('.category_name').addClass('is-invalid');
            $('#category_name_alertmsg').html('The category name is required please!').css('color','#E74A3B').css('font-weight','bolder').fadeIn('slow');
            return false;
        }else{
            $('.upload-loader').toggle();
            return true; // cancel original event to prevent form submmition
            
        }
    });
    $('form#createTasksForm').on('submit', function(){
        if($('.create_task_input').val().length < 3){
            $('.create_task_input').addClass('is-invalid');
            $('#create_task_error').html('Your issue needs some descriptions please provide some info about it.').css('color','#E74A3B').css('font-weight','bolder').fadeIn('slow');
            return false;
        }else{
            $('.upload-loader').toggle();
            return true; // cancel original event to prevent form submmition
            
        }
    });
    //createTasksForm
    $('.add-cat').click(function(){
        $('.create-categories').slideToggle(200);
    });
    $('form#createCompanyForm').on('submit', function(){
        if($('.company_name').val().length < 3){
            $('.company_name').addClass('is-invalid');
            $('#company_name_alertmsg').html('The company Name is required please!').css('color','#E74A3B').css('font-weight','bolder').fadeIn('slow');
            return false;
        }else if($('.company_manager_name').val().length < 3){
            $('.company_manager_name').addClass('is-invalid'); 
            $('#company_manager_name_alertmsg').html('The company Name is required please!').css('color','#E74A3B').css('font-weight','bolder').fadeIn('slow');
            return false;
        }else if($('.company_location').val().length < 3){
            $('.company_location').addClass('is-invalid'); 
            $('#company_location_alertmsg').html('The company Name is required please!').css('color','#E74A3B').css('font-weight','bolder').fadeIn('slow');
            return false;
        }else if($('.company_website').val().length < 3){
            $('.company_website').addClass('is-invalid'); 
            $('#company_website_alertmsg').html('The company Name is required please!').css('color','#E74A3B').css('font-weight','bolder').fadeIn('slow');
            return false;
        }else if($('.company_phone').val().length < 3){
            $('.company_phone').addClass('is-invalid'); 
            $('#company_phone_alertmsg').html('The company Name is required please!').css('color','#E74A3B').css('font-weight','bolder').fadeIn('slow');
            return false;
        }else if($('.company_email').val().length < 3){
            $('.company_email').addClass('is-invalid'); 
            $('#company_email_alertmsg').html('The company Name is required please!').css('color','#E74A3B').css('font-weight','bolder').fadeIn('slow');
            return false;
        }else if($('.company_about').val().length < 3){
            $('.company_about').addClass('is-invalid'); 
            $('#company_about_alertmsg').html('The company Name is required please!').css('color','#E74A3B').css('font-weight','bolder').fadeIn('slow');
            return false;
        }else if($('.company_existence_date').val().length < 3){
            $('.company_existence_date').addClass('is-invalid'); 
            $('#company_existence_date_alertmsg').html('The company Name is required please!').css('color','#E74A3B').css('font-weight','bolder').fadeIn('slow');
            return false;
        }else{
            $('.upload-loader').toggle();
            return true; // cancel original event to prevent form submmition
        }
      
    });
    $('.addCompany').click(function(){
        $('.create-company-form').slideToggle(200);
    });
    $('.companyclosebtn').click(function(){
        $('.create-company-form').slideToggle('slow');
    });
    $('.categoryclosebtn').click(function(){
        $('.create-categories').slideToggle('slow');
    });
    $('.taskclosebtn').click(function(){
        $('.create-task').toggle(200);
    });
    $('.taskcancelbtn').click(function(){
        $('.create-task').toggle(200);
    });
    $('.create_task').click(function(){
        $('.create-task').toggle(200);
    });
    $('[data-toggle="tooltip"]').tooltip();
  </script>
</body>

</html>
