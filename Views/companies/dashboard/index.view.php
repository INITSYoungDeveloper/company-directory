<?php
    define('APP_TITLE','Home / '.$company->name);
?>
<style>
    .categories-list{
        height: 270px;
        overflow: hidden;
        overflow-y: auto;
        margin-top: -20px;
    }
    .categories-list li{
        list-style: none;
        margin-top: 10px;
    }
</style>
<?php view('layouts/app/head'); ?>
          <!-- Page Heading -->
          <div class="d-sm-flex align-items-center justify-content-between mb-4">
            <h1 class="h6 mb-0 text-gray-800" style="opacity:0.6"><?php echo '<a href="/auth/home" >Home /</a> '. $company->name; ?></h1>
            <a href="#" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm"><i class="fas fa-upload fa-sm text-white-50"></i> Upload File</a>
          </div>

        <!-- Content Row -->
        <div class="row">
            <!-- Companies Card Example -->
            <div class="col-xl-3 col-md-6 mb-4">
                <div class="card border-left-primary shadow h-100 py-2 animated bounceInRight">
                <div class="card-body">
                    <div class="row no-gutters align-items-center">
                    <div class="col mr-2">
                        <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">Companies</div>
                        <div class="h5 mb-0 font-weight-bold text-gray-800">
                            <?php echo count($authUserCompanies); ?>
                        </div>
                    </div>
                    <div class="col-auto">
                        <i class="fas fa-calendar fa-2x text-gray-300"></i>
                    </div>
                    </div>
                </div>
                </div>
            </div>
            <!-- Projects Card Example -->
            <div class="col-xl-3 col-md-6 mb-4">
                <div class="card border-left-success shadow h-100 py-2 animated bounceInLeft">
                <div class="card-body">
                    <div class="row no-gutters align-items-center">
                    <div class="col mr-2">
                        <div class="text-xs font-weight-bold text-success text-uppercase mb-1">Projects</div>
                        <?php 
                          $query = require 'core/bootstrap.php';
                          $projects = $query->selectWhere('projects','company_id',$_GET['no']);
                        ?>
                        <div class="h5 mb-0 font-weight-bold text-gray-800"><?php echo count($projects); ?></div>
                    </div>
                    <div class="col-auto">
                        <i class="fas fa-project-diagram fa-2x text-gray-300"></i>
                    </div>
                    </div>
                </div>
                </div>
            </div>
            <!-- Earnings (Monthly) Card Example -->
            <div class="col-xl-3 col-md-6 mb-4">
                <div class="card border-left-info shadow h-100 py-2 animated bounceInRight">
                <div class="card-body">
                    <div class="row no-gutters align-items-center">
                    <div class="col mr-2">
                        <div class="text-xs font-weight-bold text-info text-uppercase mb-1">Tasks</div>
                        <div class="row no-gutters align-items-center">
                        <div class="col-auto">
                            <div class="h5 mb-0 mr-3 font-weight-bold text-gray-800">50%</div>
                        </div>
                        <div class="col">
                            <div class="progress progress-sm mr-2">
                            <div class="progress-bar bg-info" role="progressbar" style="width: 50%" aria-valuenow="90" aria-valuemin="0" aria-valuemax="100"></div>
                            </div>
                        </div>
                        </div>
                    </div>
                    <div class="col-auto">
                        <i class="fas fa-clipboard-list fa-2x text-gray-300"></i>
                    </div>
                    </div>
                </div>
                </div>
            </div>

            <!-- Pending Requests Card Example -->
            <div class="col-xl-3 col-md-6 mb-4">
                <div class="card border-left-warning shadow h-100 py-2 animated bounceInLeft">
                <div class="card-body">
                    <div class="row no-gutters align-items-center">
                    <div class="col mr-2">
                        <div class="text-xs font-weight-bold text-warning text-uppercase mb-1">Company Categories</div>
                        <div class="h5 mb-0 font-weight-bold text-gray-800"><?php echo count($categories) ?></div>
                    </div>
                    <div class="col-auto">
                        <i class="fas fa-layer-group fa-2x text-gray-300"></i>
                    </div>
                    </div>
                </div>
                </div>
            </div>
        </div>

          <!-- Content Row -->

          <div class="row">

            <!-- Area Chart -->
            <div class="col-xl-8 col-lg-7">
              <div class="card shadow mb-4">
                <!-- Card Header - Dropdown -->
                <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                  <h6 class="m-0 font-weight-bold text-primary">Projects Overview</h6>
                  <div class="dropdown no-arrow">
                    <a class="dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                      <i class="fas fa-ellipsis-v fa-sm fa-fw text-gray-400"></i>
                    </a>
                    <div class="dropdown-menu dropdown-menu-right shadow animated--fade-in" aria-labelledby="dropdownMenuLink">
                      <div class="dropdown-header">Dropdown Header:</div>
                      <a class="dropdown-item" href="#">Action</a>
                      <a class="dropdown-item" href="#">Another action</a>
                      <div class="dropdown-divider"></div>
                      <a class="dropdown-item" href="#">Help Center</a>
                    </div>
                  </div>
                </div>
                <!-- Card Body -->
                <div class="card-body">
                  <div class="chart-area">
                    <canvas id="myAreaChart"></canvas>
                  </div>
                </div>
              </div>
            </div>

            <!-- Pie Chart -->
            <div class="col-xl-4 col-lg-5">
              <div class="card shadow mb-4">
                <!-- Card Header - Dropdown -->
                <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                  <h6 class="m-0 font-weight-bold text-primary">Categories</h6>
                  <div class="dropdown no-arrow">
                    <a class="dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                      <i class="fas fa-ellipsis-v fa-sm fa-fw text-gray-400"></i>
                    </a>
                    <div class="dropdown-menu dropdown-menu-right shadow animated--fade-in" aria-labelledby="dropdownMenuLink">
                      <div class="dropdown-header">Dropdown Header:</div>
                      <a class="dropdown-item add-cat" href="javascript:void()">Add Category</a>
                      <a class="dropdown-item" id="remove_category" href="javascript:void()">Remove Category</a>
                      <div class="dropdown-divider"></div>
                      <a class="dropdown-item" href="#">Help Center</a>
                    </div>
                  </div>
                </div>
                <!-- Card Body -->
                <div class="card-body">
                  <div class="chart-pie pt-4 pb-2">
                    <!-- <canvas id="myPieChart"></canvas> -->
                    <ul class="categories-list">
                        <div class="row">
                            <div class="col-md-12">
                            <?php if(count($categories) > 0): ?>   
                              <?php foreach($categories as $category): ?>
                                <li class="row">
                                  <span>
                                    <form action="/companies/category/delete" method="POST">
                                      <input type="hidden" name="company_category_id" value="<?php echo $category->id ?>">
                                      <input style="display:none;" type="checkbox" class="delete-category-inp" onchange="this.form.submit()">
                                    </form>
                                  </span>
                                  <span class="marked"><i class="fa fa-check" aria-hidden="true"></i> </span>
                                  <span style="margin-top: -5px;margin-left:6px;"><?php echo $category->name ?></span>
                                </li>
                              <?php endforeach; ?>
                            <?php else: ?>
                            <h5 style="opacity:0.5">No category available!</h5>
                            <?php endif; ?>
                            </div>
                            
                        </div>
                        
                    </ul>
                  </div>
                  <div class="mt-4 text-center small">
                    <span class="mr-2">
                      <!-- <i class="fas fa-circle text-primary"></i> Direct -->
                    </span>
                    <span class="mr-2">
                      <!-- <i class="fas fa-circle text-success"></i> Social -->
                    </span>
                    <span class="mr-2">
                      <!-- <i class="fas fa-circle text-info"></i> Referral -->
                    </span>
                  </div>
                </div>
              </div>
            </div>
          </div>

          <!-- Content Row -->
          <div class="row">

            <!-- Content Column -->
            <div class="col-lg-6 mb-4">

              <!-- Project Card Example -->
              <div class="card shadow mb-4">
                <div class="card-header py-3">
                  <h6 class="m-0 font-weight-bold text-primary">Projects Watchout <span class=" h6 text-gray-800">(Time Estimation)</span> </h6>

                </div>
                <div class="card-body">
                  <?php
                    $query = require 'core/bootstrap.php';
                    $projects = $query->selectWhere('projects','company_id',$_GET['no']);
                    if(count($projects) > 0):
                    foreach($projects as $project):
                    if($project->completed == false):
                    $project_start_date = $project->project_starting_date;
                    $project_deadline_date = $project->project_deadline;
                    $currentDate = date("Y-m-d"); //The current date 
                    // $currentDate = '2019-7-30';
                    $initialDayInterval = intval(round(abs(strtotime($project_start_date) - strtotime($project_deadline_date))/86400)); //Days in the strat date and the deadline date
                    $presentDayInterval = intval(round(abs(strtotime($currentDate) - strtotime($project_start_date))/86400)); //Days in the start date and the current date
                    $remainingDays =  $initialDayInterval - $presentDayInterval;  //Get the remaining days left
                    // echo $presentDayInterval . '|'.$initialDayInterval;
                    if($initialDayInterval !== 0):
                      if($presentDayInterval < $initialDayInterval OR $presentDayInterval == $initialDayInterval):
                      $timeEstimationPercentage = $presentDayInterval / $initialDayInterval * 100;  //Get days in percentage
                      else:
                        $timeEstimationPercentage = 100;
                      endif;
                    else:
                      $timeEstimationPercentage = 1;
                    endif;
                    // 
                  ?>
                  <h4 class="small font-weight-bold"><?php echo $project->name ?> <span class="float-right">
                  <?php echo intval($timeEstimationPercentage); ?>% (<?php if($remainingDays < 1):?>  Your time is up!<?php else: echo $remainingDays.'days remaining'; endif; ?>)</span></h4>
                  <div class="progress mb-4">
                    <!-- Progress bar -->
                    <div class="progress-bar <?php if($timeEstimationPercentage < 40 AND $timeEstimationPercentage > 20){ echo 'bg-warning';}elseif($timeEstimationPercentage < 20 AND $timeEstimationPercentage > 0){ echo 'bg-danger';}elseif($timeEstimationPercentage < 80 AND $timeEstimationPercentage > 60){echo 'bg-primary';}elseif($timeEstimationPercentage < 60 AND $timeEstimationPercentage > 40){echo 'bg-info';}elseif($timeEstimationPercentage > 80){echo 'bg-success';}?>" role="progressbar" style="width: <?php echo $timeEstimationPercentage ?>%" aria-valuenow="<?php echo $timeEstimationPercentage ?>" aria-valuemin="0" aria-valuemax="100"></div>
                  </div>
                  <?php else: ?>
                  <?php $info = '<h6>Good job! You have completed some of your projects</h6>' ?>
                  <?php endif; ?>
                  <?php endforeach; ?>
                  <?php if(isset($info)){echo $info;} ?> 
                  <?php else: ?>
                  <h6>Oops! You dont have any project created yet!</h6>
                  <?php endif; ?>
                </div>
              </div>
            </div>
            <div class="col-lg-6 mb-4">
              <!-- Illustrations -->
              <div class="card shadow mb-4">
                <div class="card-header py-3">
                  <h6 class="m-0 font-weight-bold text-primary">Updates</h6>
                </div>
                <div class="card-body">
                  <div class="text-center">
                    <img class="img-fluid px-3 px-sm-4 mt-3 mb-4" style="width: 25rem;" src="../Public/app/img.1/undraw_posting_photo.svg" alt="">
                  </div>
                  <p>Add some quality, when you're adding your project, give a good and well descriptive name for the project so that when other users sees it, they can understand what has been built by your company <b><?php echo $company->name; ?></b></p>
                  <a target="_blank" rel="nofollow" href="/companies/index?no=<?php echo $company->id ?>">Browse your page here &rarr;</a>
                </div>
              </div>
              
            </div>
          </div>
        </div>
        <!-- /.container-fluid -->
      </div>
      <!-- End of Main Content -->
      <?php 
     
      view('layouts/app/bottom'); ?>
      <script>
        $('#remove_category').click(function(){
          $('.delete-category-inp').toggle('slow');
          $('.marked').toggle('slow');
        });
        // Main --------------Chart JS---------------
        // Set new default font family and font color to mimic Bootstrap's default styling
Chart.defaults.global.defaultFontFamily = 'Nunito', '-apple-system,system-ui,BlinkMacSystemFont,"Segoe UI",Roboto,"Helvetica Neue",Arial,sans-serif';
Chart.defaults.global.defaultFontColor = '#858796';

function number_format(number, decimals, dec_point, thousands_sep) {
  // *     example: number_format(1234.56, 2, ',', ' ');
  // *     return: '1 234,56'
  number = (number + '').replace(',', '').replace(' ', '');
  var n = !isFinite(+number) ? 0 : +number,
    prec = !isFinite(+decimals) ? 0 : Math.abs(decimals),
    sep = (typeof thousands_sep === 'undefined') ? ',' : thousands_sep,
    dec = (typeof dec_point === 'undefined') ? '.' : dec_point,
    s = '',
    toFixedFix = function(n, prec) {
      var k = Math.pow(10, prec);
      return '' + Math.round(n * k) / k;
    };
  // Fix for IE parseFloat(0.55).toFixed(0) = 0;
  s = (prec ? toFixedFix(n, prec) : '' + Math.round(n)).split('.');
  if (s[0].length >3) {
    s[0] = s[0].replace(/\B(?=(?:\d{3})+(?!\d))/g, sep);
  }
  if ((s[1] || '').length < prec) {
    s[1] = s[1] || '';
    s[1] += new Array(prec - s[1].length + 1).join('0');
  }
  return s.join(dec);
}

// Area Chart Example
var ctx = document.getElementById("myAreaChart");
var myLineChart = new Chart(ctx, {
  type: 'line',
  data: {
    labels: ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"],
    datasets: [{
      label: "Completed Projects",
      lineTension: 0.2,
      backgroundColor: "rgba(78, 115, 223, 0.05)",
      borderColor: "rgba(78, 115, 223, 1)",
      pointRadius: 3,
      pointBackgroundColor: "rgba(78, 115, 223, 1)",
      pointBorderColor: "rgba(78, 115, 223, 1)",
      pointHoverRadius: 3,
      pointHoverBackgroundColor: "rgba(78, 115, 223, 1)",
      pointHoverBorderColor: "rgba(78, 115, 223, 1)",
      pointHitRadius: 10,
      pointBorderWidth: 2,
      data: [
        <?php echo count($jan);  ?>, 
        <?php echo count($feb);  ?>, 
        <?php echo count($mar);  ?>, 
        <?php echo count($apr);  ?>,
        <?php echo count($may);  ?>, 
        <?php echo count($jun);  ?>, 
        <?php echo count($jul);  ?>, 
        <?php echo count($aug);  ?>, 
        <?php echo count($sep);  ?>, 
        <?php echo count($oct);  ?>, 
        <?php echo count($nov);  ?>, 
        <?php echo count($dec);  ?>
      ],
    }],
  },
  options: {
    maintainAspectRatio: false,
    layout: {
      padding: {
        left: 10,
        right: 25,
        top: 25,
        bottom: 0
      }
    },
    scales: {
      xAxes: [{
        time: {
          unit: 'date'
        },
        gridLines: {
          display: false,
          drawBorder: false
        },
        ticks: {
          maxTicksLimit: 7
        }
      }],
      yAxes: [{
        ticks: {
          maxTicksLimit: 5,
          padding: 10,
          // Include a dollar sign in the ticks
          callback: function(value, index, values) {
            return '' + number_format(value);
          }
        },
        gridLines: {
          color: "rgb(234, 236, 244)",
          zeroLineColor: "rgb(234, 236, 244)",
          drawBorder: false,
          borderDash: [2],
          zeroLineBorderDash: [2]
        }
      }],
    },
    legend: {
      display: false
    },
    tooltips: {
      backgroundColor: "rgb(255,255,255)",
      bodyFontColor: "#858796",
      titleMarginBottom: 10,
      titleFontColor: '#6e707e',
      titleFontSize: 14,
      borderColor: '#dddfeb',
      borderWidth: 1,
      xPadding: 15,
      yPadding: 15,
      displayColors: false,
      intersect: false,
      mode: 'index',
      caretPadding: 10,
      callbacks: {
        label: function(tooltipItem, chart) {
          var datasetLabel = chart.datasets[tooltipItem.datasetIndex].label || '';
          return datasetLabel + ': ' + number_format(tooltipItem.yLabel);
        }
      }
    }
  }
});

        //---------Chart JS--------------------------
      </script>