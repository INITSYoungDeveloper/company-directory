<!DOCTYPE html>
<html lang="en">
<head>

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>Create Project</title>

  <!-- Custom fonts for this template-->
  <link href="/Public/app/vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
  <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">
  <link href="/Public/app/css.1/style.css" rel="stylesheet">
  <!-- Custom styles for this template-->
  <link href="/Public/assets/css/animate.css" rel="stylesheet">
  <link href="/Public/app/css.1/sb-admin-2.min.css" rel="stylesheet">
    <style>
        body{
            height: 100%;
        }
        .img{
            width: 500px;
            margin-top: 30px;
        }
        .main-container{
            margin-top: 80px;
        }
        .form-group{
            display: block;
        }
        label{
            font-size: 13px;
        }
        select{
            border: 2px solid #DFE1E6;
            /* padding: 6px 0px 0px 6px; */
            max-width: 100%;
            background: #FAFBFC;
            line-height: 20px;
            font-size: 14px;
            height: 40px;
            width: 350px;
            color: #9098A9;
            border-top: none;
            margin: 10px 0px 13px 0px;
            border-right: none;
            border-left: none;
            outline: none;
            cursor: pointer;
        }
        select option{
            border: none;
            background: #FAFBFC;
            outline: none;
        }
        .project_about:focus{
            height: 150px !important;
        }
        .create-btn{
            background: #EBECF0;
            border: none;
            font-size: 14px;
            color: #A5ADBA;
        }
        .submit-btn{
            text-align: right;

        }
    </style>

</head>
<body class="animated fadeInDown">
    <div class="container">
        <div class="row main-container">
            <div class="col-md-6 img-container">
                <img src="/Public/assets/img/create-pro.png" alt="project showcase image" class="img" alt="">
            </div>
            <div class="col-md-6 form-container">
                <h3 style="margin-left: 5px;font-weight: lighter;">Create Project</h3><br>
                <div class="col-sm-8">
                <div class="form">
                    <form action="/companies/project/create" id="createProjectFormField" method="POST">
                        <input type="hidden" name="company_id" value="<?php echo $_GET['no']; ?>">
                    <div class="form-group animated bounceInRight">
                        <label for="inp" class="inp">
                            <input type="text" autocomplete="off" active id="inp" class="project_name" required name="project_name" placeholder="&nbsp;">
                            <span class="label">Name</span>
                            <span class="border"></span>
                        </label>
                        <div class="project_name_error"></div>
                    </div>
                    <div class="form-group  animated bounceInLeft">
                        <!-- PM -->
                        <select name="project_mang" id="type" class="project_mang">
                        <option class="type" value="Project Manager:">Project Manager:</option>
                            <?php foreach($pmInfo as $pm): ?>
                                <option value="<?php echo $pm->id ?>">@<?php echo $pm->firstname; ?></option>
                            <?php endforeach; ?>
                        </select>
                        <div class="project_lead_error"></div>
                    </div>
                    <div class="form-group animated bounceInRight">
                        <!-- PM -->
                        <select name="project_type" id="type" class="project_type">
                            <option class="type" value="Type:">Type:</option>
                            <option value="Software">Software (default)</option>
                        </select>
                        <div class="project_type_error"></div>
                    </div>
                    <div class="form-group  animated bounceInLeft">
                        <div class="row">
                            <div class="col-md-6">
                                <label for="inp" class="inp">
                                    <input type="date" required id="inp" class="project_start_date" name="project_start_date" placeholder="&nbsp;">
                                    <span class="label">Start Date</span>
                                    <span class="border"></span>
                                </label>
                            </div>
                            <div class="col-md-6">
                                <label for="inp" class="inp">
                                    <input type="date" required id="inp" class="project_deadline_date" name="project_deadline_date" placeholder="&nbsp;">
                                    <span class="label">Deadline Date</span>
                                    <span class="border"></span>
                                </label>
                            </div>
                        </div>
                    </div>
                    <div class="form-group animated bounceInRight">
                        <label for="inp" class="inp">
                            <textarea id="inp" class="project_about" required name="project_about" placeholder="&nbsp;"></textarea>
                            <span class="label">About</span>
                            <span class="border"></span>
                        </label>
                        <div class="project_about_error"></div>
                    </div>
                    <div class="form-group submit-btn">

                        <input type="submit" class="btn btn-secondary create-btn animated bounceInDown" style="opacity: 0.3;" disabled value="Create">
                    </div>
                    </form>
                </div>
                </div>
            </div>
            <div class="loader" style="display:none">
            <img style="bottom:0px" class="upload-loader-file"  src="/Public/assets/img/load.gif"> Please Wait
          </div>
        </div>
    </div>
      <!-- Bootstrap core JavaScript-->
  <script src="/Public/app/vendor/jquery/jquery.min.js"></script>
  <script src="/Public/app/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
  <script>
     $('#inp').keyup(function(){
            if($(this).val().length !=0)
                $('.create-btn').attr('disabled', false).css('opacity','1');  
                            
            else
                $('.create-btn').attr('disabled',true).css('opacity','0.3');
        })
      $('form#createProjectFormField').submit(function(){
        // $('.loader').toggle('slow');
          if($('.project_name').val().length < 3){
            $('.project_name_error').html("Your project needs a name!").css('color','darkred').css('font-size','13px').css('font-weight','bolder');
            return false;
          }else if($('.project_mang').val() == 'Project Manager:'){
            $('.project_lead_error').html("Your project needs a project manager's name!").css('color','darkred').css('font-size','13px').css('font-weight','bolder');
            return false;
          }else if($('.project_type').val() == 'Type:'){
            $('.project_type_error').html("What type of project are you working on?").css('color','darkred').css('font-size','13px').css('font-weight','bolder');
            return false;
          }else if($('.project_about').val().length < 3){
            $('.project_about_error').html("Please give some title to the project").css('color','darkred').css('font-size','13px').css('font-weight','bolder');
            return false;
          }else{
            return true;
          }
      });
  </script>
</body>