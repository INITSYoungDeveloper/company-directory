<?php
    define('APP_TITLE','Home / '.$company->name.' / Projects');
?>
<style>
    .categories-list{
        height: 270px;
        overflow: hidden;
        overflow-y: auto;
        margin-top: -20px;
    }
    .categories-list li{
        list-style: none;
        margin-top: 10px;
    }
    
.toggle{
  --uiToggleSize: var(--toggleSize, 20px);
  --uiToggleIndent: var(--toggleIndent, .4em);
  --uiToggleBorderWidth: var(--toggleBorderWidth, 2px);
  --uiToggleColor: var(--toggleColor, #000);
  --uiToggleDisabledColor: var(--toggleDisabledColor, #868e96);
  --uiToggleBgColor: var(--toggleBgColor, #4E73DF);
  --uiToggleArrowWidth: var(--toggleArrowWidth, 2px);
  --uiToggleArrowColor: var(--toggleArrowColor, #4E73DF);

  /* display: inline-block; */
  position: relative;
  margin: auto;
  text-align: center;
}

.toggle__input{
  position: absolute;
  left: -99999px;
}

.toggle__label{
  display: inline-flex;
  cursor: pointer;
  min-height: var(--uiToggleSize);
  padding-left: calc(var(--uiToggleSize) + var(--uiToggleIndent));
}

.toggle__label:before, .toggle__label:after{
  content: "";
  box-sizing: border-box;  
  width: 0.8em;
  height: 0.8em;
  font-size: var(--uiToggleSize);
  position: absolute;
  left: 0;
  top: 0;
}

.toggle__label:before{
  border: var(--uiToggleBorderWidth) solid #4E73DF;
  z-index: 2;
}

.toggle__input:disabled ~ .toggle__label:before{
  border-color: silver;
}

.toggle__input:focus ~ .toggle__label:before{
  box-shadow: 0 0 0 2px silver, 0 0 0px 4px silver;
}

.toggle__input:not(:disabled):checked:focus ~ .toggle__label:after{
  box-shadow: 0 0 0 2px silver, 0 0 0px 4px silver;
}

.toggle__input:not(:disabled) ~ .toggle__label:after{
  background-color: silver;
  opacity: 0;
}

.toggle__input:not(:disabled):checked ~ .toggle__label:after{
  opacity: 1;
}

.toggle__text{
  margin-top: auto;
  margin-bottom: auto;
}

/*
The arrow size and position depends from sizes of square because I needed an arrow correct positioning from the top left corner of the element toggle
*/

.toggle__text:before{
  content: "";
  box-sizing: border-box;
  width: 0;
  height: 0;
  font-size: var(--uiToggleSize);

  border-left-width: 0;
  border-bottom-width: 0;
  border-left-style: solid;
  border-bottom-style: solid;
  border-color: #FAFBFC;

  position: absolute;
  top: .5428em;
  left: .2em;
  z-index: 3;

  transform-origin: left top;
  transform: rotate(-40deg) skew(10deg);
}

.toggle__input:not(:disabled):checked ~ .toggle__label .toggle__text:before{
  width: .5em;
  height: .25em;
  border-left-width: var(--uiToggleArrowWidth);
  border-bottom-width: var(--uiToggleArrowWidth);
  will-change: width, height;
  transition: width .1s ease-out .2s, height .2s ease-out;
  
}

/*
=====
LEVEL 2. PRESENTATION STYLES
=====
*/

/* 
The demo skin
*/

.toggle__label:before, .toggle__label:after{
  border-radius: 2px;

}

/* 
The animation of switching states
*/

.toggle__input:not(:disabled) ~ .toggle__label:before,
.toggle__input:not(:disabled) ~ .toggle__label:after{
  opacity: 1;
  transform-origin: center center;
  will-change: transform;
  transition: transform .2s ease-out;
}

.toggle__input:not(:disabled) ~ .toggle__label:before{
  transform: rotateY(0deg);
  transition-delay: .2s;
}

.toggle__input:not(:disabled) ~ .toggle__label:after{
  transform: rotateY(90deg);
}

.toggle__input:not(:disabled):checked ~ .toggle__label:before{
  transform: rotateY(-90deg);
  transition-delay: 0s;
}

.toggle__input:not(:disabled):checked ~ .toggle__label:after{
  transform: rotateY(0deg);
  transition-delay: .2s;
}

.toggle__text:before{
  opacity: 0;
}

.toggle__input:not(:disabled):checked ~ .toggle__label .toggle__text:before{
  opacity: 1;
  transition: opacity .1s ease-out .3s, width .1s ease-out .5s, height .2s ease-out .3s;
}

/*
=====
LEVEL 3. SETTINGS
=====
*/

.toggle{
  --toggleColor: #4E73DF;
  --toggleBgColor: #4E73DF;
  --toggleSize: 50px;
}

</style>
<?php view('layouts/app/head'); ?>
    <!-- Page Heading -->
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <h1 class="h6 mb-0 text-gray-800" style="opacity:0.6"><?php echo APP_TITLE ?></h1>
        <a href="/companies/project/create?no=<?php echo $_GET['no'] ?>" target="_blank" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm"> Create Project <i class="fa fa-angle-down fa-sm text-white-50" aria-hidden="true"></i></a>
    </div>
     <!-- Content Row -->
     <div class="row">
            <!-- Companies Card Example -->
            <div class="col-xl-3 col-md-6 mb-4 animated slideInRight">
                <div class="card border-left-primary shadow h-100 py-2">
                <div class="card-body">
                    <div class="row no-gutters align-items-center">
                    <div class="col mr-2">
                        <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">Companies</div>
                        <div class="h5 mb-0 font-weight-bold text-gray-800">
                            <?php echo count($companies); ?>
                        </div>
                    </div>
                    <div class="col-auto">
                        <i class="fas fa-calendar fa-2x text-gray-300"></i>
                    </div>
                    </div>
                </div>
                </div>
            </div>
            <!-- Projects Card Example -->
            <div class="col-xl-3 col-md-6 mb-4 animated slideInLeft">
                <div class="card border-left-success shadow h-100 py-2">
                <div class="card-body">
                    <div class="row no-gutters align-items-center">
                    <div class="col mr-2">
                        <div class="text-xs font-weight-bold text-success text-uppercase mb-1">Projects</div>
                        <div class="h5 mb-0 font-weight-bold text-gray-800"><?php echo count($companyProjects); ?></div>
                    </div>
                    <div class="col-auto">
                        <i class="fas fa-project-diagram fa-2x text-gray-300"></i>
                    </div>
                    </div>
                </div>
                </div>
            </div>
            <!-- Earnings (Monthly) Card Example -->
            <div class="col-xl-3 col-md-6 mb-4 animated slideInRight">
                <div class="card border-left-info shadow h-100 py-2">
                <div class="card-body">
                    <div class="row no-gutters align-items-center">
                    <div class="col mr-2">
                        <div class="text-xs font-weight-bold text-info text-uppercase mb-1">Tasks</div>
                        <div class="row no-gutters align-items-center">
                        <div class="col-auto">
                            <div class="h5 mb-0 mr-3 font-weight-bold text-gray-800">50%</div>
                        </div>
                        <div class="col">
                            <div class="progress progress-sm mr-2">
                            <div class="progress-bar bg-info" role="progressbar" style="width: 50%" aria-valuenow="90" aria-valuemin="0" aria-valuemax="100"></div>
                            </div>
                        </div>
                        </div>
                    </div>
                    <div class="col-auto">
                        <i class="fas fa-clipboard-list fa-2x text-gray-300"></i>
                    </div>
                    </div>
                </div>
                </div>
            </div>

            <!-- Pending Requests Card Example -->
            <div class="col-xl-3 col-md-6 mb-4 animated slideInLeft">
                <div class="card border-left-warning shadow h-100 py-2">
                <div class="card-body">
                    <div class="row no-gutters align-items-center">
                    <div class="col mr-2">
                        <div class="text-xs font-weight-bold text-warning text-uppercase mb-1">Company Categories</div>
                        <div class="h5 mb-0 font-weight-bold text-gray-800"><?php echo count($categories) ?></div>
                    </div>
                    <div class="col-auto">
                        <i class="fas fa-layer-group fa-2x text-gray-300"></i>
                    </div>
                    </div>
                </div>
                </div>
            </div>
        </div>
        <div class="table-content">
            <!-- DataTales Example -->
          <div class="card shadow mb-4 border-left-primary">
            <div class="card-header py-3 ">
              <h6 class="m-0 font-weight-bold text-primary">Projects Entries</h6>
            </div>
            <div class="card-body">
                <?php if(count($companyProjects) > 0): ?>
                    <div class="table-responsive">
                        <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0" style="border: none;">
                        <thead>
                            <tr>
                            <th>Project Name</th>
                            <th>Type</th>
                            <th>Lead</th>
                            <th>Completed</th>
                            <th>Start date</th>
                            <th>Deadline</th>
                            </tr>
                        </thead>
                        <tfoot>
                            <tr>
                            <th>Project Name</th>
                            <th>Type</th>
                            <th>Lead</th>
                            <th>Completed</th>
                            <th>Start date</th>
                            <th>Deadline</th>
                            </tr>
                        </tfoot>
                        <tbody>
                        <?php foreach($companyProjects as $project):?>
                            <tr>
                            
                                <td><a href="/projects/index?no=<?php echo $project->id ?>" class="text-dark"><?php echo $project->name ?></a></td>
                                <td> <span class="badge-primary" style="border-radius:10px;padding: 2px;font-size:10px"><i class="fa fa-code" aria-hidden="true"></i></span>  <?php echo $project->type ?></td>
                                <td>
                                  <?php 
                                   $query = require 'core/bootstrap.php';
                                    $pm = $query->selectWhere('project_managers', 'id', $project->lead)[0]; 
                                  ?>
                                  <a  data-toggle="tooltip" data-html="true" title="<?php echo $pm->email ?><h6><?php echo $pm->firstname.' '.$pm->lastname ?></h6>" data-placement="top" href="javascript:void()">@<?php echo $pm->firstname ?></a>
                                  
                                </td>
                                <td>
                                  
                                    <form action="/companies/projects/updates" id="projectUpdateForm" method="POST">
                                      <input type="hidden" name="project_id" value="<?php echo $project->id ?>">
                                      <label class="toggle">
                                      <?php if($project->completed): ?>
                                          <input class="toggle__input" name="completed" checked type="checkbox" onchange="this.form.submit()">
                                      <?php  else: ?>
                                        <input class="toggle__input" name="completed" type="checkbox" onchange="this.form.submit()">
                                      <?php endif; ?>
                                          <span class="toggle__label">
                                            <span class="toggle__text"></span>
                                          </span>
                                      </label>
                                    </form>

                                </td>
                                <td><?php echo $project->project_starting_date ?></td>
                                <td><?php echo $project->project_deadline ?></td>
                            
                            </tr>
                            <?php endforeach; ?>
                        </tbody>
                        </table>
                    </div>
                <?php  else:?>
                    No project is available yet!
                <?php endif; ?>
            </div>
          </div>
        </div>
<!-- End of Main Content -->
<?php view('layouts/app/bottom'); ?>
<script>
    $('form#projectUpdateForm').on('submit', function(){
        $.ajax({ // create an AJAX call...
                data: $(this).serialize(), // get the form data
                type: $(this).attr('method'), // GET or POST
                url: $(this).attr('action'), // the file to call
                success: function(response) { // on success..
                    $('#pageloadcontent .card-body').html(response); // update the DIV
                },
                error: function(e){
                    console.error(e);
                }
            });
            return false; // cancel original event to prevent form submmition
        });
    
</script>