<link rel="stylesheet" href="/Public/css/issue.css">
<?php
$taskArray = array('todo','indevelopment','readyforqa','intesting', 'done');
// var_dump($taskArray);
// exit;
define('APP_TITLE',"Projects / $project->name ");
    view('layouts/app/head'); ?>
    Projects / <?php echo $project->name ?>
    <!-- <h1 class="h3"></h1>  -->
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
            <h1 class="h3 mb-0 text-gray-800" style="opacity:0.6">Issue Board</h1>
            <a href="javascript:void()" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm create_task"> Create Task</a>
          </div>
    <div class="container issue-board">
        <div class="rows">
            <div class="col-md-2d">
                <div class="main-top-list">
                    <h3 class="h5">To Do</h3>
                </div>
            </div>
            <div class="col-md-3d">
                <div class="main-top-list">
                    <h3 class="h5">In Development</h3>
                </div>
            </div>
            <div class="col-md-3d">
                <div class="main-top-list">
                    <h3 class="h5">READY FOR QA</h3>
                </div>
            </div>
            <div class="col-md-3d">
                <div class="main-top-list">
                    <h3 class="h5">IN TESTING PHASE</h3>
                </div>
            </div>
            <div class="col-md-3d">
                <div class="main-top-list">
                    <h3 class="h5">DONE</h3>
                </div>
            </div>
        </div>
        <div class="row" style="margin-top: 10px">
          <div class="container">
                <i class="fa fa-angle-down" aria-hidden="true"></i>
                <span class="issue-building-logo"><i class="fa fa-building" aria-hidden="true"></i></span>
                <?php echo $project->name ?> issues
          </div>
        </div>
        <div class="rows main-list-rows">
          <?php
          require('Controller/IssuesController.php');
          $issue = new Issues();
          foreach($taskArray as $task_name): ?>
            <div class="main-list" id="<?php echo $task_name ?>">

              <?php
              $issues = $issue->index($task_name);
               if(isset($issues) && !empty($issues)):  ?>
               <?php foreach($issues as $task): ?>
               
                <div style="cursor: move;" class="<?php if($task_name == 'done'): echo 'border-left-success'; endif; ?> card shadow mb-4" id="<?php echo $task->id; ?>">
                <!-- Card Header - Dropdown -->
                    <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                    <h6 class="m-0 font-weight-light text-secondary"> <i class="fa fa-calendar" aria-hidden="true"></i> <?php echo $task->created_at ?></h6>
                    <div class="dropdown no-arrow">
                        <a class="dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <i class="fas fa-ellipsis-v fa-sm fa-fw text-gray-400"></i>
                        </a>
                        <div class="dropdown-menu dropdown-menu-right shadow animated--fade-in" aria-labelledby="dropdownMenuLink" x-placement="bottom-end" style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(17px, 18px, 0px);">
                        <div class="dropdown-header">Info Header:</div>
                        <a class="dropdown-item deleteTask" id="<?php echo $task->id ?>" href="javascript:void()">Delete</a>
                        <!-- <a class="dropdown-item" href="#"></a> -->
                        <div class="dropdown-divider"></div>
                        <a class="dropdown-item text-primary" href="javascript:void()">FAQ</a>
                        </div>
                    </div>
                    </div>
                    <!-- Card Body -->
                    <div class="card-body">
                    <?php  echo $task->issue;?>
                    </div>
                </div>
                <!-- Card ends -->
                
                <?php endforeach; ?>
               <?php  else:?>
                
                    
                <?php  endif;?>
            </div>
          <?php endforeach; ?>
        </div>
        
    </div>
<?php view('layouts/app/bottom'); ?>
<script>
    $(document).ready(function(){
        $('.deleteTask').click(function(){
            var id = $(this).attr('id');
            // Ajax call
            $.ajax({
                url: '/projects/issue/delete',
                type: 'GET',
                data:{
                    'id': id
                },
                'success': function(){
                    $('.card#'+ id).hide(200);
                    $('#pageloadcontent').slideToggle();
                    $('#pageloadcontent .card-body').html('Issue Deleted successfully!');
                },
                error: function(e){
                    console.error(e);
                }
            });
        });
        $('.main-list .card').draggable({
            revert: true
        });
        $('.main-list').droppable({
            tolerance: 'pointer',
            drop: function(event, ui){
                var id= $(ui.draggable).attr('id');
                var task = $(ui.draggable).attr('html');
                var type = $(this).attr('id');
                // if(type == 'done'){
                //     $('.main-list .card').addClass('border-left-success');
                // }
                $.ajax({
                    url: '/projects/issue/update',
                    type: 'GET',
                    data: {
                        'id': id,
                        'type': type
                    },
                    'success' : function(){
                        $('#' + type).append(ui.draggable); 
                    },
                    error: function(e){
                        console.error(e);
                    }
                });
            }
        });
    });
</script>