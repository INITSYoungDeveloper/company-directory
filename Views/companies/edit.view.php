<?php  
define('APP_TITLE','Edit | '.$company->name);
view('layouts/app/head');

?>
<style>
    .form-control{
        font-size: 14px;
        background: #f8F9FC;
        border:none;
    }
    label{
        font-size: 14px;
    }
    input.btn{
        font-size: 14px;
    }
    .bk-dash{
        text-align: right !important;
        float: right;
        margin-top: 7px;
    }

</style>
    <div class="card mb-4 py-2 border-left-info">
        <div class="card-body">
           <b> Note: </b>You will need to either add or replace any info. Make sure nothing is empty in the required fields!
        </div>
    </div>


    <div class="edit-form-field card border-left-primary">
        <div class="card-header text-primary" style="font-weight: lighter">
           <?php echo $company->name ?> / Edit
        </div>
        <div class="container">
        <form action="/companies/edit" method="POST">
            <input type="hidden" name="company_id" value="<?php echo $company->id ?>">
            <br>
            <div class="row">
 
                <div class="col-md-6 ">
                    <div class="form-group mb-3">
                        <label>Name:</label>
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <div class="input-group-text bg-primary text-light"> <i class="far fa-newspaper fa-fw"></i></div>
                            </div>
                            <input type="text" class="form-control" name="company_name" value="<?php echo $company->name ?>">
                        </div>
                        <span></span>
                    </div> 
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="">Managing Director's name:</label>
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <div class="input-group-text bg-primary text-light"> <i class="far fa-user fa-fw"></i></div>
                            </div>
                            <input type="text" class="form-control" name="company_manager_name" value="<?php echo $company->company_manager_name ?>">
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="">Location:</label>
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <div class="input-group-text bg-primary text-light"> <i class="fas fa-map-marker-alt"></i></div>
                            </div>
                            <input type="text" class="form-control" name="company_location" value="<?php echo $company->location ?>">
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="">Net</label>
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <div class="input-group-text bg-primary text-light"> <i class="fa fa-location-arrow" aria-hidden="true"></i></div>
                                    </div>
                                    <input type="text" class="form-control" value="https://">
                                </div>
                            </div>
                        </div>
                        <div class="col-md-8">
                            <div class="form-group">
                                <label for="">Website:</label>
                                <input type="text" class="form-control" name="company_website" value="<?php  echo $company->website?>">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="">Email:</label>
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <div class="input-group-text bg-primary text-light"><i class="far fa-envelope-open"></i></div>
                            </div>
                            <input type="text" class="form-control" name="company_email" value="<?php echo $company->email ?>">
                        </div>
                        
                    </div>
                </div>
                <div class="col-md-6">
                        
                        <div class="form-group">
                            <label for="">Phone:</label>
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <div class="input-group-text bg-primary text-light"><i class="fas fa-phone"></i></div>
                                </div>
                                <input type="text" class="form-control" name="company_phone" value="<?php echo $company->phone ?>">
                            </div>
                           
                        </div>  
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="">About:</label>
                        <textarea style="height: 93px;" is-primary name="company_about" id="" cols="30" rows="10" class="form-control border-bottom-primary"><?php echo $company->about ?></textarea>
                    </div>
                    
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="">Existed since:</label>
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <div class="input-group-text bg-primary text-light"><i class="far fa-envelope-open"></i></div>
                            </div>
                            <input type="date" class="form-control" name="company_existed_since" value="<?php echo $company->start_date ?>">
                        </div><br>
                            
                            <input type="submit" class="btn btn-primary btn-sm" value="Save changes">
                            <!-- <div> -->
                            <a class="bk-dash" href="/auth/home">← Back to Dashboard</a>
                            <!-- </div> -->
                    </div>
                </div>
            </div>
        </div>
        </form>
    </div>
<?php  view('layouts/app/bottom');?>