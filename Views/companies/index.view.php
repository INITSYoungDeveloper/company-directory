<?php session_start(); ?>
<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>ComCloud | <?php echo $company->name ?></title>

  <!-- Bootstrap core CSS -->
  <link href="/Public/comp/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">
  <!-- Custom styles for this template -->
  <link href="/Public/app/vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
  <link href="/Public/comp/css/modern-business.css" rel="stylesheet">
    <style>
        html{
            scroll-behavior: smooth;
        }
        body{
            transition: 2s ease-in;
            scroll-behavior: smooth;
            font-family: Nunito,-apple-system,BlinkMacSystemFont,"Segoe UI",Roboto,"Helvetica Neue",Arial,sans-serif,"Apple Color Emoji","Segoe UI Emoji","Segoe UI Symbol","Noto Color Emoji" !important;
        }
        .image {
            display: block;
            width: 100%;
            height: auto;
        }
        .text small{
            font-family: Nunito, -apple-system, BlinkMacSystemFont, "Segoe UI", Roboto, "Helvetica Neue", Arial, sans-serif, "Apple Color Emoji", "Segoe UI Emoji", "Segoe UI Symbol", "Noto Color Emoji" !important;
        }
        .overlay {
            position: absolute;
            bottom: 0;
            left: 0;
            right: 0;
            background-color: #4E73DF;
            overflow: hidden;
            width: 0;
            height: 100%;
            transition: .5s ease;
            margin: auto;
            border-radius: 3px;
            opacity: 0.9
        }

        .mainl:hover .overlay {
            width: 100%;
            /* height: 100%; */

        }
        .name{
            border: 1px solid white;
            padding: 5px 10px;
            border-radius: 3px;
            opacity: 0.5
        }
        .scroll-spy{
            padding: 0px 12px;
            opacity: 0.8;
            font-size: 30px;
            background: #4E73DF;
            position: absolute;
            cursor: pointer;
            right: 10px;
            border-radius: 3px;
            color: white !important;
            transition: .5s;
        }
        .scroll-spy:hover{
            transition: .5s;
            opacity: 1;
        }
        .text {
            color: white;
            font-weight: bolder;
            font-size: 20px;
            position: absolute;
            top: 50%;
            left: 50%;
            -webkit-transform: translate(-50%, -50%);
            -ms-transform: translate(-50%, -50%);
            transform: translate(-50%, -50%);
            white-space: nowrap;
            text-align: center
            
        }
        .carousel-txt{
            transition: 10s;
            animation: jump 4s linear infinite;
        }
        @keyframes jump{
            0%{
                transition: 1s ease-in;
                transform: translateY(-20px);
            }
            50%{
                transition: 1s ease-in;
                transform: translateY(-10px);
            }
            100%{
                transition: 1s ease-in;
                transform: translateY(-20px);
            }
        }
        .dashboard{
            background: #4E73DF;
            color: white !important;
            border-radius: 3px;
            opacity: 0.9;
        }
    </style>

</head>

<body id="body">

  <!-- Navigation -->
  <nav class="navbar fixed-top navbar-expand-lg navbar-light bg-light fixed-top" style="box-shadow:0 .15rem 1.75rem 0 rgba(58,59,69,.15)!important;background: white !important;">
    <div class="container">
      <a class="navbar-brand" href="index.html"><?php echo $company->name; ?></a>
      <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
      <div class="collapse navbar-collapse" id="navbarResponsive">
        <ul class="navbar-nav ml-auto">
          <li class="nav-item">
            <a class="nav-link dropdown-toggle" href="#about">About</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="#about">Services</a>
          </li>
          
          <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle" href="#portfolio" id="navbarDropdownPortfolio"  aria-haspopup="true" aria-expanded="false">
              Portfolio
            </a>
            
          </li>
          <?php if($_SESSION['user_id'] == $company->user_id): ?>
          <li class="nav-item">
            <a class="nav-link dashboard" href="/companies/dashboard?no=<?php echo $company->id; ?>" target="_blank">Dashboard</a>
          </li>
          <?php endif; ?>
        </ul>
      </div>
    </div>
  </nav>

  <header>
    <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
      <ol class="carousel-indicators">
        <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
        <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
        <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
      </ol>
      <div class="carousel-inner" role="listbox">
        <!-- Slide One - Set the background image for this slide in the line below -->
        <div class="carousel-item active" style="background-image: url('../Public/comp/img/top-3.gif')">
          <div class="carousel-caption d-none d-md-block carousel-txt" style="color: darkgreen;">
            <h3><?php echo $company->name ?></h3>
            <p style="opacity:0.5;"><i>Managing Director: <?php echo ucwords($company->company_manager_name) ?></i></p>
          </div>
        </div>
        <!-- Slide Two - Set the background image for this slide in the line below -->
        <div class="carousel-item" style="background-image: url('../Public/comp/img/top-1.gif')">
          <div class="carousel-caption d-none d-md-block carousel-txt">
            <h3><?php echo ucwords($company->name) ?></h3>
            <p style="opacity:0.5;"><i>Managing Director: <?php echo ucwords($company->company_manager_name) ?></i></p>
          </div>
        </div>
        <!-- Slide Three - Set the background image for this slide in the line below -->
        <div class="carousel-item" style="background-image: url('../Public/comp/img/top-2.gif')">
          <div class="carousel-caption d-none d-md-block">
            <h3><?php echo ucwords($company->name) ?></h3>
            <p style="opacity:0.5;"><i>Managing Director: <?php echo ucwords($company->company_manager_name) ?></i></p>
          </div>
        </div>
      </div>
      <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
        <span  class="carousel-control-prev-icon" aria-hidden="true"></span>
        <span class="sr-only">Previous</span>
      </a>
      <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
        <span class="carousel-control-next-icon" aria-hidden="true"></span>
        <span class="sr-only">Next</span>
      </a>
    </div>
  </header>
  <!-- Page Content -->
  <div class="container">

    <h1 class="my-4" id="portfolio" style="text-align: right;">Welcome to Modern Business</h1>

<br><br>

    <!-- Portfolio Section -->
    <h2 >Portfolio Heading <button class="btn btn-sm btn-secondary" style="border-radius: 20px">All</button></h2>

    <div class="row">
      <div class="col-lg-4 col-sm-6 portfolio-item">
        <div class="card h-90 mainl">
            <div class="card-body image">
                <h4 class="card-title">
                <a href="#">Project One</a>
                </h4>
                <p class="card-text">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Amet numquam aspernatur eum quasi sapiente nesciunt? Voluptatibus sit, repellat sequi itaque deserunt, dolores in, nesciunt, illum tempora ex quae? Nihil, dolorem!</p>
            </div>
            <div class="overlay">
                <div class="text"><div class="name"><?php echo $company->name ?></div><br/> <small><i class="fas fa-clipboard-check"></i> Completed</small></div>
                
            </div>
        </div>
      </div>
      <div class="col-lg-4 col-sm-6 portfolio-item">
        <div class="card h-90 mainl">
            <div class="card-body image">
                <h4 class="card-title">
                <a href="#">Project Two</a>
                </h4>
                <p class="card-text">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Amet numquam aspernatur eum quasi sapiente nesciunt? Voluptatibus sit, repellat sequi itaque deserunt, dolores in, nesciunt, illum tempora ex quae? Nihil, dolorem!</p>
            </div>
            <div class="overlay">
                <div class="text"><div class="name"><?php echo $company->name ?></div><br/> <small><i class="fas fa-clipboard-check"></i> Completed</small></div>
                
            </div>
        </div>
      </div>
      <div class="col-lg-4 col-sm-6 portfolio-item">
        <div class="card h-90 mainl">
            <div class="card-body image">
                <h4 class="card-title">
                <a href="#">Project Three</a>
                </h4>
                <p class="card-text">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Amet numquam aspernatur eum quasi sapiente nesciunt? Voluptatibus sit, repellat sequi itaque deserunt, dolores in, nesciunt, illum tempora ex quae? Nihil, dolorem!</p>
            </div>
            <div class="overlay">
                <div class="text"><div class="name"><?php echo $company->name ?></div><br/> <small><i class="fas fa-clipboard-check"></i> Completed</small></div>
                
            </div>
        </div>
      </div>
      <div class="col-lg-4 col-sm-6 portfolio-item">
        <div class="card h-90 mainl">
            <div class="card-body image">
                <h4 class="card-title">
                <a href="#">Project Four</a>
                </h4>
                <p class="card-text">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Amet numquam aspernatur eum quasi sapiente nesciunt? Voluptatibus sit, repellat sequi itaque deserunt, dolores in, nesciunt, illum tempora ex quae? Nihil, dolorem!</p>
            </div>
            <div class="overlay">
                <div class="text"><div class="name"><?php echo $company->name ?></div><br/> <small><i class="fas fa-clipboard-check"></i> Completed</small></div>
                
            </div>
        </div>
      </div>
      <div class="col-lg-4 col-sm-6 portfolio-item">
        <div class="card h-90 mainl">
            <div class="card-body image">
                <h4 class="card-title">
                <a href="#">Project Five</a>
                </h4>
                <p class="card-text">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Amet numquam aspernatur eum quasi sapiente nesciunt? Voluptatibus sit, repellat sequi itaque deserunt, dolores in, nesciunt, illum tempora ex quae? Nihil, dolorem!</p>
            </div>
            <div class="overlay">
                <div class="text"><div class="name"><?php echo $company->name ?></div><br/> <small><i class="fas fa-clipboard-check"></i> Completed</small></div>
                
            </div>
        </div>
      </div>
      <div class="col-lg-4 col-sm-6 portfolio-item">
        <div class="card h-90 mainl">
            <div class="card-body image">
                <h4 class="card-title">
                <a href="#">Project Six</a>
                </h4>
                <p class="card-text">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Amet numquam aspernatur eum quasi sapiente nesciunt? Voluptatibus sit, repellat sequi itaque deserunt, dolores in, nesciunt, illum tempora ex quae? Nihil, dolorem!</p>
            </div>
            <div class="overlay">
                <div class="text"><div class="name"><?php echo $company->name ?></div><br/> <small><i class="fas fa-clipboard-check"></i> Completed</small></div>
                
            </div>
        </div>
      </div>
    </div>
    <!-- /.row -->

    <!-- Features Section -->
    <div class="row">
      <div class="col-lg-6">
        <h2 id="about">Modern Business Features</h2>
        <p>The Modern Business categories includes:</p>
        <ul>
          <li>
            <strong>Clone</strong>
          </li>
          <li>Blaur</li>
          <li>Brawx</li>
          <li>Working contact form with validation</li>
          <li>Unstyled page elements for easy customization</li>
        </ul>
        <p>
        <?php echo $company->about; ?>
        </p>
      </div>
      <div class="col-lg-6 mainl">
        <img class="img-fluid image rounded" src="/Public/comp/img/about-1.gif" alt="">
        <div class="overlay">
            <div class="text"><div class="name"><?php echo $company->name ?></div></div>
        </div>
      </div>
    </div>
    <!-- /.row -->

    <hr>

    <!-- Call to Action Section -->
    <div class="row mb-4">
      <div class="col-md-8">
        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Molestias, expedita, saepe, vero rerum deleniti beatae veniam harum neque nemo praesentium cum alias asperiores commodi.</p>
      </div>
      <div class="col-md-4">
        <a class="btn btn-lg btn-secondary btn-block" href="#">Call to Action</a>
      </div>
    </div>
    <a class="scroll-spy" href="#body">
    <i class="fas fa-angle-up"></i>
    </a>
  </div>
  <!-- /.container -->
  <!-- Footer -->
  <footer class="py-5 bg-light">
    <div class="container">
      <p class="m-0 text-center text-small my-auto text-secondary">Copyright &copy; ComCloud 2019</p>
    </div>
    <!-- /.container -->
  </footer>

  <!-- Bootstrap core JavaScript -->
  <script src="/Public/comp/vendor/jquery/jquery.min.js"></script>
  <script src="/Public/comp/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

</body>

</html>
