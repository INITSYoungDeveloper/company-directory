<?php  
define('APP_TITLE','Edit | '.$pmInfo->firstname);
view('layouts/app/head');

?>
<style>
    .form-control{
        font-size: 14px;
        background: #f8F9FC;
        border:none;
    }
    label{
        font-size: 14px;
    }
    input.btn{
        font-size: 14px;
    }
    .bk-dash{
        text-align: right !important;
        float: right;
        margin-top: 7px;
    }

</style>
    <div class="card mb-4 py-2 border-left-info">
        <div class="card-body">
           <b> Note: </b>You will need to either add or replace any info. Make sure nothing is empty in the required fields!
        </div>
    </div>


    <div class="edit-form-field card border-left-primary">
        <div class="card-header text-primary" style="font-weight: lighter">
           @<?php echo $pmInfo->firstname ?> / Edit
        </div>
        <div class="container">
        <form action="/project/m/edit" method="POST">
            <input type="hidden" name="pm_id" value="<?php echo $pmInfo->id ?>">
            <br>
            <div class="row">
                <div class="col-md-6 ">
                    <div class="form-group mb-3">
                        <label>Firstname:</label>
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <div class="input-group-text bg-primary text-light"> <i class="far fa-newspaper fa-fw"></i></div>
                            </div>
                            <input type="text" class="form-control" name="pm_firstname" value="<?php echo $pmInfo->firstname ?>">
                        </div>
                        <span></span>
                    </div>
                </div>
            </div>
            <div class="row">
            <div class="col-md-6">
                    <div class="form-group">
                        <label for="">Lastname:</label>
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <div class="input-group-text bg-primary text-light"> <i class="far fa-user fa-fw"></i></div>
                            </div>
                            <input type="text" class="form-control" name="pm_lastname" value="<?php echo $pmInfo->lastname ?>">
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="">Email:</label>
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <div class="input-group-text bg-primary text-light"><i class="far fa-envelope-open"></i></div>
                            </div>
                            <input type="text" class="form-control" name="pm_email" value="<?php echo $pmInfo->email; ?>">
                        </div>
                        
                    </div>
                </div>
                
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group" style="margin-top: 30px;">
                        <input type="submit" class="btn btn-primary btn-sm" value="Save changes">
                       
                    </div>
                </div>
                <div class="col-md-6">
                <div class="form-group" style="margin-top: 30px;">
                    <a class="bk-dash" href="/auth/home">← Back to Dashboard</a>
                </div>
                
                </div>
            </div>
            
        </div>
        </form>
    </div>
<?php  view('layouts/app/bottom');?>