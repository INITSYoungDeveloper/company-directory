
<?php 
define('APP_TITLE','Project Managers');
view('layouts/app/head'); ?>
    <!-- Main content -->
    <div class="d-sm-flex align-items-center justify-content-between mb-4">     
        <h1 class="h5 mb-0 text-gray-800" style="opacity:0.6"><i class="fas fa-fw fa-chart-area"></i><?php echo APP_TITLE ?></h1>
        <a href="/project/m/create" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm"><i class="fas fa-upload fa-sm text-white-50"></i> Add Project Manager</a>
    </div>
    <div>
             <!-- Content Row -->
     <!-- <div class="row"> -->
        
        <div class="table-content">
            <!-- DataTales Example -->
          <div class="card shadow mb-4 border-left-primary">
            <div class="card-header py-3 ">
              <h6 class="m-0 font-weight-bold text-primary">PM Info</h6>
            </div>
            <div class="card-body">
                <?php if(count($project_managers) > 0): ?>
                    <div class="table-responsive">
                        <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0" style="border: none;">
                        <thead>
                            <tr>
                            <th>Firstname</th>
                            <th>Lastname</th>
                            <th>Email</th>
                            <th>Modify</th>
                            </tr>
                        </thead>
                        <tfoot>
                            <tr>
                            <th>Firstname</th>
                            <th>Lastname</th>
                            <th>Email</th>
                            <th>Modify</th>
                            </tr>
                        </tfoot>
                        <tbody>
                            <?php foreach($project_managers as $pm):?>
                            <tr>
                                <td><?php echo $pm->firstname ?></td>
                                <td><?php echo $pm->lastname ?></td>
                                <td> <?php echo $pm->email; ?></td>
                               
                                <td class="row" style="text-align: center;margin: auto"><button class="btn btn-secondary btn-sm"><a style="color: white" href="/project/m/edit?no=<?php echo $pm->id;?>">Edit</a></button> 
                                    <div style="margin-left:5px;" class="editCompanyFormField">
                                        <form action="/project/m/delete" method="POST">
                                            <input type="hidden" name="pm_id" id="" value="<?php echo $pm->id ?>">
                                            <input type="submit" class="btn btn-danger btn-sm"  value="Delete">
                                        </form>
                                    </div>
                                </td>
                            </tr>
                            <?php endforeach; ?>
                        </tbody>
                        </table>
                    </div>
                <?php  else:?>
                    No Company Account is created yet! <br>Please create one in the left dropdown lists or click on the button at the top of this information board.<br> Thanks!
                <?php endif; ?>
            </div>
          </div>
        </div>
    </div>
<!-- Bottom contents -->
<?php view('layouts/app/bottom'); ?>