<!DOCTYPE html>
<html lang="en">
<head>

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>Create PM</title>

  <!-- Custom fonts for this template-->
  <link href="/Public/app/vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
  <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">
  <link href="/Public/app/css.1/style.css" rel="stylesheet">
  <!-- Custom styles for this template-->
  <link href="/Public/assets/css/animate.css" rel="stylesheet">
  <link href="/Public/app/css.1/sb-admin-2.min.css" rel="stylesheet">
    <style>
        body{
            height: 100%;
        }
        .img{
            width: 500px;
            margin-top: 30px;
        }
        .main-container{
            margin-top: 80px;
        }
        .form-group{
            display: block;
        }
        label{
            font-size: 13px;
        }
        select{
            border: 2px solid #DFE1E6;
            max-width: 100%;
            background: #FAFBFC;
            line-height: 20px;
            font-size: 14px;
            height: 40px;
            width: 350px;
            color: #9098A9;
            border-top: none;
            margin: 10px 0px 13px 0px;
            border-right: none;
            border-left: none;
            outline: none;
            cursor: pointer;
        }
        select option{
            border: none;
            background: #FAFBFC;
            outline: none;
        }
        .project_about:focus{
            height: 150px !important;
        }
        .create-btn{
            background: #EBECF0;
            border: none;
            font-size: 14px;
            color: #A5ADBA;
        }
        .submit-btn{
            text-align: right;
        }
    </style>
</head>
<body class="animated fadeInDown">
    <div class="container">
        <div class="row main-container">
            <div class="col-md-6 img-container">
                <img src="/Public/app/img.1/1.png" alt="project showcase image" class="img" alt="">
            </div>
            <div class="col-md-6 form-container">
                <h3 style="margin-left: 5px;font-weight: lighter;">Project Manager</h3><br>
                <div class="col-sm-8">
                <div class="form">
                    <form action="/project/m/create" id="createProjectFormField" method="POST">
                    <div class="form-group animated bounceInRight">
                        <label for="inp" class="inp">
                            <input type="text" autocomplete="off" active id="inp" class="pm_firstname" required name="pm_firstname" placeholder="&nbsp;">
                            <span class="label">Firstname</span>
                            <span class="border"></span>
                        </label>
                        <div class="pm_firstname_error"></div>
                    </div>
                    <div class="form-group  animated bounceInLeft">
                        <!-- PM -->
                        <label for="inp" class="inp">
                            <input type="text" autocomplete="off" active id="inp" class="pm_lastname" required name="pm_lastname" placeholder="&nbsp;">
                            <span class="label">Lastname</span>
                            <span class="border"></span>
                        </label>
                        <div class="pm_lastname_error"></div>
                    </div>
                    <div class="form-group animated bounceInRight">
                        <!-- PM -->
                        <label for="inp" class="inp">
                            <input type="text" autocomplete="off" active id="inp" class="pm_email" required name="pm_email" placeholder="&nbsp;">
                            <span class="label">Email</span>
                            <span class="border"></span>
                        </label>
                        <div class="pm_email_error"></div>
                    </div>
                    <div class="form-group animated bounceInRight">
                        <!-- PM -->
                        <select name="pm_company_id" id="type" class="pm_company_id">
                            <option class="type" value="Company:">Company:</option>
                            <?php foreach($authorizedUsercompanies as $company): ?>
                                <option value="<?php echo $company->id ?>"><?php echo $company->name ?></option>
                            <?php endforeach; ?>
                        </select>
                        <div class="pm_company_id_error"></div>
                    </div>
                    
                    <div class="form-group submit-btn">

                        <input type="submit" class="btn btn-secondary create-btn animated bounceInDown" style="opacity: 0.3;" disabled value="Create">
                    </div>
                    </form>
                </div>
                </div>
            </div>
            <div class="loader" style="display:none">
            <img style="bottom:0px" class="upload-loader-file"  src="/Public/assets/img/load.gif"> Please Wait
          </div>
        </div>
    </div>
      <!-- Bootstrap core JavaScript-->
  <script src="/Public/app/vendor/jquery/jquery.min.js"></script>
  <script src="/Public/app/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
  <script>
  
     $('#inp').keyup(function(){
        if($(this).val().length !=0)
            $('.create-btn').attr('disabled', false).css('opacity','1');  
        else
            $('.create-btn').attr('disabled',true).css('opacity','0.3');
      })
      $('form#createProjectFormField').submit(function(){
          if($('.pm_firstname').val().length < 3){
            $('.pm_firstname_error').html("Firstname is required please!").css('color','darkred').css('font-size','13px').css('font-weight','bolder');
            return false;
          }else if($('.pm_lastname').val().length < 3){
            $('.pm_lastname_error').html("Lastname is required please!").css('color','darkred').css('font-size','13px').css('font-weight','bolder');
            return false;
          }else if($('.pm_email').val().length < 3){
            $('.pm_email_error').html("An email address is required please!").css('color','darkred').css('font-size','13px').css('font-weight','bolder');
            return false;
          }else if($('.pm_company_id').val() == 'Company:'){
            $('.pm_company_id_error').html("Please select a value from the lists").css('color','darkred').css('font-size','13px').css('font-weight','bolder');
            return false;
          }else{
            return true;
          }
      });
  </script>
</body>