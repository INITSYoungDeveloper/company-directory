<!-- Top contents -->
<?php  
    define('APP_TITLE','COMPANIES');
    view('layouts/app/head');
?>
<!-- Main content -->
    <div class="d-sm-flex align-items-center justify-content-between mb-4">     
        <h1 class="h3 mb-0 text-gray-800" style="opacity:0.6"><i class="fa fa-home" aria-hidden="true"></i><?php echo APP_TITLE ?></h1>
        <a href="javascript:void()" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm addCompany"><i class="fas fa-upload fa-sm text-white-50"></i> Create Company</a>
    </div>
    <div>
             <!-- Content Row -->
     <!-- <div class="row"> -->
        
        <div class="table-content">
            <!-- DataTales Example -->
          <div class="card shadow mb-4 border-left-primary">
            <div class="card-header py-3 ">
              <h6 class="m-0 font-weight-bold text-primary">Companies Info</h6>
            </div>
            <div class="card-body">
                <?php if(count($allCompanies) > 0): ?>
                    <div class="table-responsive">
                        <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0" style="border: none;">
                        <thead>
                            <tr>
                            <th>Company Name</th>
                            <th>Managing Director</th>
                            <th>Email</th>
                            <th>Website</th>
                            <th>Start date</th>
                            <th>Modify</th>
                            </tr>
                        </thead>
                        <tfoot>
                            <tr>
                            <th>Company Name</th>
                            <th>Managing Director</th>
                            <th>Email</th>
                            <th>Website</th>
                            <th>Start date</th>
                            <th>Modify</th>
                            </tr>
                        </tfoot>
                        <tbody>
                        <?php foreach($allCompanies as $company):?>
                            <tr>
                                <td> <a class="text-black-50" href="/companies/dashboard?no=<?php echo $company->id; ?>"> <?php echo $company->name ?></a></td>
                                <td><?php echo $company->company_manager_name ?></td>
                                <td> <?php echo $company->email; ?></td>
                                <td>
                                  <a href="//<?php echo $company->website ?>">  <?php echo $company->website ?></a>
                                </td>
                                <td><?php echo $company->start_date ?></td>
                                <td class="row" style="text-align: center;margin: auto"><button class="btn btn-secondary btn-sm"><a style="color: white" href="/companies/edit?no=<?php echo $company->id;?>">Edit</a></button> 
                                    <div style="margin-left:5px;" class="editCompanyFormField">
                                        <form action="/companies/delete" method="POST">
                                            <input type="hidden" name="company_id" id="" value="<?php echo $company->id ?>">
                                            <input type="submit" class="btn btn-danger btn-sm"  value="Delete">
                                        </form>
                                    </div>
                                </td>
                            </tr>
                            <?php endforeach; ?>
                        </tbody>
                        </table>
                    </div>
                <?php  else:?>
                    No Company Account is created yet! <br>Please create one in the left dropdown lists or click on the button at the top of this information board.<br> Thanks!
                <?php endif; ?>
            </div>
          </div>
        </div>
    </div>
<!-- Bottom contents -->
<?php
    view('layouts/app/bottom');
?>