<?php

class LoginController{
    
    public function userLogin(){
        return view('auth/login');
    }
    public function userLoginAuthentication(){
        session_start();
        // Auth::middleware('auth');
        $query = require 'core/bootstrap.php';   //Querybuilder
        $password = request('password');
        $email = request('email');
        if(count($query->checkIfUserAuthExists('users','email',$email)) > 0 AND count($query->checkIfUserAuthExists('users','password',$password)) > 0){
            $user_details = $query->selectWhere('users','email',$email); //Select the users details
            $user_data = $user_details[0];
            if(isset($_SESSION['user_firstname']) && !empty($_SESSION['user_firstname'])){
                // Redirect to homepage if user already logged in
                event_caller('success','You are logged in already '.$_SESSION['firstname'].'!');  
                return redirect('/auth/home');   
            }else{
                createSessions([
                    'user_firstname' => $user_data->firstname,
                    'user_lastname' => $user_data->lastname,
                    'user_phone' => $user_data->phone,
                    'user_email_address' => $user_data->email_address,
                    'user_location' => $user_data->location,
                    'user_gender' => $user_data->gender,
                    'user_id' => $user_data->id
                ]);
                event_caller('success','Welcome Back '.$user_data->firstname.'!');  
                return redirect('/auth/home');
            }
        }else{
            event_caller('error',"Username or Password not correct. Please provide a valid information! <br> Thanks!");
            return back();
        }   //Sniks out
    }
    public function forgotPassword(){
        return view('auth/lost_password');
    }
    public function updateUserWithPassword(){
        $email = request('email');
        $password = request('password');
        $confirm_password = request('confirm_password');
        if(isset($email) && empty($email)){
            event_caller('error','Your Email Address is required!');
            return back();
        }elseif(isset($password) && empty($password)){
            event_caller('error','Your New password is required!');
            return back();
        }elseif($password !== $confirm_password){
            event_caller('error','Your passwords doesn\'t match, please confirm it');
            return back();
        }elseif(strlen($password) < 6){
            event_caller('error','Password must have atleast 6 characters!');
            return back();
        }else{
            $query = require 'core/bootstrap.php';  
            $user = $query->selectWhere('users','email',$email);
            if(isset($user) && !empty($user)){
                $query->update('users',[
                    'password' => $password
                ],$user[0]->id);
                event_caller('success','You have successfully changed your password. Please login with your informations');
                return redirect('/auth/login');
            }else{
                event_caller('error','You don\'t have an account with us. Kindly register with your valid informations');
                return back();
            }
        }
    }
    public function userLoginWithGoogle(){
        session_start();
        require_once("core\auth.php");
            if(isset($_GET['code'])){
                $gClient->authenticate($_GET['code']);
                $token = $gClient->getAccessToken();
                $_SESSION['token'] = $token;
            }
            if(isset($_SESSION['token'])){
                $gClient->setAccessToken($_SESSION['token']);
            }
            if($gClient->getAccessToken()){
                $userData = $google_oauthv2->userinfo->get();
                // var_dump($userData);
                // exit;
                if(isset($userData) && !empty($userData)){
                   
                    // $picture = $userData['picture'];
                    // $given_name = $userData['given_name'];
                    $user_email_address = $userData['email'];
                    session_start();
                    createSessions([
                        'google_email_address' => $user_email_address
                    ]);
                    return redirect('/auth/google/login/verify');
                    
                }else{
                    event_caller('error','Login not successful. Please try again later!');
                    return redirect('/auth/login');
                }
            }
        
    }
    public function userLoginWithGoogleVerification(){
        session_start();
        $email = $_SESSION['google_email_address'];
        if(isset($email) && !empty($email)){
            $query = require 'core/bootstrap.php';
            $user = $query->selectWhere('users','email',$email);
            
            if(isset($user) && !empty($user)){
               
                    createSessions([
                        'user_firstname' => $user[0]->firstname,
                        'user_lastname' => $user[0]->lastname,
                        'user_email_address' => $user[0]->email,
                        'user_location' => $user[0]->location,
                        'user_gender' => $user[0]->gender,
                        'user_id' => $user[0]->id,
                        'user_phone' => $user[0]->phone
                    ]);
                    event_caller('success','Welcome back '.$user[0]->firstname.'!');
                    return redirect('/auth/home');
            }else{
                event_caller('error','Your informations doesn\'t match our records. Please login or register');
                return redirect('/auth/login');
            }
        }
        
    }
}