<?php

class ErrorsController{
    /**
     * @return
     * Error Page
     */
    public function Error404(){
        session_start();
        return view('ErrorPage/404');
    }
   
}