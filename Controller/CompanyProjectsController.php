<?php

class CompanyProjectsController{
    public function index(){
        session_start();
        $query = require 'core/bootstrap.php';
        $companies = $query->selectAll('companies');
        $categories = $query->selectAll('categories');
        $company_id = $_GET['no'];
        
        $companies = $query->selectWhere('companies','id',$company_id);
        
        if($companies){
            foreach($companies as $comp){
                $company = $comp;
            }
            $companyProjects = $query->selectWhere('projects','company_id',$company_id);
            return view('companies/dashboard/projects/index',compact('companies','company','categories','companyProjects'));
        }else{
            // event_caller('error','You have no permission to that page!');
            redirect('/error/404');
        }
       
    }
    // public function 
    public function createProject(){
        $company_id = $_GET['no'];
        $query = require 'core/bootstrap.php';
        $companies = $query->selectWhere('companies','id',$company_id);
        if($companies){
            $pmInfo = $query->selectWhere('project_managers','company_id', $company_id);
            return view('companies/dashboard/projects/create',compact('company_id','pmInfo'));
        }else{
            
            return redirect('/error/404');
        }
       
    }
    public function validateRequestData(){
        $query = require 'core/bootstrap.php';
        session_start();
        $firstname = $_SESSION['user_firstname'];
        $query->insert('projects',[
            'name' => ucwords(request('project_name')),
            'about' => request('project_about'),
            'lead' => ucwords(request('project_mang')),
            'project_starting_date' => request('project_start_date'),
            'project_deadline' => request('project_deadline_date'),
            'type' => request('project_type'),
            'company_id' => request('company_id'),
            'completed' => false
        ]);
        $query->insert('notifications',[
            'title' => 'A new project has just been created by '.strtoupper($firstname).'!',
            'type' => 'project'
        ]);
        event_caller('success','Project created!');
        return redirect('/auth/home');
    }
    public function showProjectInfo(){
        session_start();
        $user_name = $_SESSION['user_firstname'];
        
        if(isset($user_name) && !empty($user_name)){
            $query = require 'core/bootstrap.php';
            $project_id = $_GET['no'];
            $projects = $query->selectWhere('projects','id', $project_id);
            if($projects){
                foreach($projects as $proj){
                    $project = $proj;
                }
                return view('companies/dashboard/projects/show',compact('project'));
            }else{
                event_caller('error','You have no permission to that page!');
                return redirect('/error/404');
            }
        }else{
            event_caller('error','You will need to login first to have access to tht page');
            return redirect('/auth/login');
        }
        
        
    }
    public function updateProjectIfCompleted(){
        session_start();
        $user_firstname = $_SESSION['user_firstname'];
        $project_id = request('project_id');
        $completed = request('completed');
        $query = require 'core/bootstrap.php';
        $project = $query->selectWhere('projects','id',$project_id)[0];
        if($completed == 'on'){
            $query->update('projects',[
                'completed' => true
            ],$project_id);
            $query->insert('graph',[
                'company_id' => $project->company_id,
                'month' => date('F'),
                'project_id' => $project->id
            ]);
            event_caller('success','You have successfully completed '.$project->name);
            
        }else{
            $query->update('projects',[
                'completed' => false
            ],$project_id);
            $query->destroy('graph','project_id',$project->id);
            event_caller('success','You have successfully marked '.$project->name.' as incompleted project!');
        }
        $query->insert('notifications',[
            'title' => 'Project information has just been updated by '.strtoupper($user_firstname).'!',
            'type' => 'project'
        ]);
        
        return back();

    }
    public function createTask(){
        echo "<script>alert('Hello there')</script>";
        $query = require 'core/bootstrap.php';
        $query->insert('issues',[
            'title' => 'todo',
            'issue' => request('issue'),
            'project_id' => request('project_id'),
            'created_at' => date('Y-m-d')
        ]);
        event_caller('success','Issue has been added successfully!');
        return back();
    }
    public function updateTask(){
        $issue_id = $_GET['id'];
        $type = $_GET['type'];
        $query = require 'core/bootstrap.php';
        $query->update('issues',[
            'title' => $type,
        ],$issue_id);
        
    }
    public function deleteTask(){
        $issue_id = $_GET['id'];
        $query = require 'core/bootstrap.php';
        $query->delete('issues',$issue_id);
    }

}