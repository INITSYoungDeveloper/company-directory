<?php

class CompaniesController{
    public function index(){
        $id = $_GET['no'];
        $query = require 'core/bootstrap.php';
        $companies = $query->selectWhere('companies','id',$id);
        if($companies){
            foreach($companies as $comp){
                $company = $comp;
            }
            return view('companies/index',compact('company'));
        }else{
            // event_caller('error','You have no permission to that page!');
            return redirect('/error/404');
        }
        
    }
    public function editCompanyInfo(){
        session_start();
        if(isset($_SESSION['user_firstname'])){
            $id = $_GET['no'];
            $query = require 'core/bootstrap.php';
            $companies = $query->selectWhere('companies','id',$id);
            if($companies){
                foreach($companies as $comp){
                    $company = $comp;
                }
                return view('companies/edit',compact('company'));
            }else{
                // event_caller('error','You have no permission to that page!');
                return redirect('/error/404');
            }
        }else{
            return redirect('/error/404');
        }
    }
    public function updateCompanyInfo(){
        $query = require 'core/bootstrap.php';
        $query->update('companies',[
            'name' => request('company_name'),
            'company_manager_name' => request('company_manager_name'),
            'email' => request('company_email'),
            'location' => request('company_location'),
            'phone' => request('company_phone'),
            'about' => request('company_about'),
            'website' => request('company_website'),
            'start_date' => request('company_existed_since')
        ],request('company_id'));
        event_caller('success','Changes made successfully');    
        return redirect('/auth/companies');
    }
    public function createAccount(){
        session_start();
        $query = require 'core/bootstrap.php';
        if(isset($_SESSION['user_firstname'])){
            $user_id = $_SESSION['user_id'];
            $firstname = $_SESSION['user_firstname'];
            $query->insert('companies',[
                'name' => strtoupper(request('company_name')),
                'company_manager_name' => ucwords(request('company_manager_name')),
                'email' => request('company_email'),
                'phone' => request('company_phone'),
                'about' => request('company_about'),
                'website' => request('company_website'),
                'location' => request('company_location'),
                'start_date' => request('company_existence_date'),
                'user_id' => $user_id
            ]);
            $query->insert('notifications',[
                'title' => 'A new company has just been created by '.strtoupper($firstname).'!',
                'type' => 'company'
            ]);
            event_caller('success',"You have successfully created ".strtoupper(request('company_name')).'!');
            return back();
        }else{
            event_caller('error', 'You need to login before you perform this action <br> Thanks!');
            return redirect('/auth/login');
        }
    }
    public function createNewUserInput(){
        
    }
    public function createCompanyCategories(){
        session_start();
        $query = require 'core/bootstrap.php';
        $firstname = $_SESSION['user_firstname'];
        $query->insert('categories',[
            'name' => ucwords(request('category_name')),
            'company_id' => request('company_id')
        ]);
        $query->insert('notifications',[
            'title' => 'A new company category has just been added by '.strtoupper($firstname).'!',
            'type' => 'category'
        ]);
        event_caller('success',"You have successfully created ".request('category_name').'!');
        return back();
    }
    public function showUserCompanies(){
        session_start();
        $query = require 'core/bootstrap.php';
        $user_id = $_SESSION['user_id'];
        $allCompanies = $query->selectWhere('companies','user_id',$user_id);
        return view('companies/all',compact('allCompanies'));
    }
    public function create(){
        return view('User/register');
    }
    public function store(){
        $query = require 'core/bootstrap.php';
        // Get all the post requests
        $user_name = request('user_name');
        $user_image = $_FILES['user_img'];
        $user_phone = request('user_phone');
        $user_gender = request('user_gender');
        $user_email = request('user_email');
        $user_job = request('job');
        $user_street_name = request('user_street_name');
        $user_street_number = request('user_street_number');
        $user_city = request('user_city');
        $user_country = request('user_country');
        $company_name = request('company_name');
        $company_created_by = request('created_by');
        $company_phone = request('company_phone');
        $company_registered_on = request('registered_on');
        $company_location = request('company_location');
        $company_email = request('company_email');
        $company_website = request('company_website');
        $company_file = $_FILES['company_file'];
        $project_name = request('project_name');
        $about_project = request('about_project');
        $project_starting_date = request('project_starting_date');
        $project_deadline_date = request('project_deadline_date');
        $project_presentation_date = request('project_presentation_date');
        $project_discussions = request('discussions');
        $project_file = $_FILES['project_file'];
        $user_image_name = $user_image['name'];
        $user_location = $user_street_number. ' '. $user_street_name.' '.$user_city. ' '.$user_country;
        if(isset($company_file) && !empty($company_file)){
            $c_file = [];
            $p_file = [];
            for($i=0;$i<count($company_file['name']);$i++){
                $c_file[] = [
                    'name' => $company_file['name'][$i],
                    'size' => $company_file['size'][$i],
                    'type' => $company_file['type'][$i],
                    'tmp_name' => $company_file['tmp_name'][$i]
                ];
            }
            for($i=0;$i<count($project_file['name']);$i++){
                $p_file[] = [
                    'name' => $project_file['name'][$i],
                    'size' => $project_file['size'][$i],
                    'type' => $project_file['type'][$i],
                    'tmp_name' => $project_file['tmp_name'][$i]
                ];
            }
        }
        
        $query->insert('users',[
            'fullname' => $user_name,
            'email_address' => $user_email,
            'phone' => $user_phone,
            'gender' => $user_gender,
            'location' => $user_location,
            'field' => $user_job,
            'image' => $user_image_name
        ]);
        
        $company_user_id = $query->show('users','email_address',$user_email);   //Get user id
        if(isset($company_user_id) && !empty($company_user_id)){
            foreach($company_user_id as $comp_user_id){
                $user_id = $comp_user_id;
            }
            if(isset($user_id) && !empty($user_id)){
                $query->insert('companies',[
                    'company_name' => $company_name,
                    'email_address' => $company_email,
                    'phone_number' => $company_phone,
                    'location' => $company_location,
                    'website' => $company_website,
                    'created_by' => $company_created_by,
                    'registered_on' => $company_registered_on,
                    'user_id' => $user_id->id
                ]);
                $company_ids = $query->show('companies','user_id',$user_id->id);        //Get company id
                foreach($company_ids as $comp_id){
                    $company_id = $comp_id;
                }
                if(isset($company_id->id) && !empty($company_id->id)){
                    foreach($c_file as $com_file){
                        if(!empty($com_file['name']) && !empty($com_file['size']) && !empty($com_file['type'])){
                            $query->insert('files',[
                                'name' => $com_file['name'],
                                'size' => $com_file['size'],
                                'type' => $com_file['type'],
                                'company_id' => $company_id->id
                            ]);
                        }
                    }
                    $query->insert('projects',[
                        'company_id' => $company_id->id,
                        'project_name' => $project_name,
                        'discussions' => $project_discussions,
                        'about' => $about_project,
                        'deadline' => $project_deadline_date,
                        'started_on' => $project_starting_date,
                        'presented_on' => $project_presentation_date
                    ]);
                    $project_ids = $query->show('projects','company_id',$company_id->id);           //Get project id
                    foreach($project_ids as $proj_id){
                        $project_id = $proj_id->id;
                    }
                    if(isset($project_id) && !empty($project_id)){
                        foreach($p_file as $proj_file){
                            if(!empty($proj_file['name']) && !empty($proj_file['size']) && !empty($proj_file['type'])){
                                $query->insert('project_files',[
                                    'name' => $proj_file['name'],
                                    'size' => $proj_file['size'],
                                    'type' => $proj_file['type'],
                                    'project_id' => $project_id
                                ]);
                            }
                        }
                        if(!empty($p_file) && !empty($c_file)){
                            require_once('core/driveUpload.php');
                            if (isset($_GET['code'])) {
                                $_SESSION['accessToken'] = $client->authenticate($_GET['code']);
                                $client->setAccessToken($_SESSION['accessToken']);
                                $service = new Google_DriveService($client);
                                // $finfo = finfo_open(FILEINFO_MIME_TYPE);
                                $file = new Google_DriveFile();
                                
                                foreach ($c_file as $comp_file) {
                                    if(!empty($comp_file['name']) && !empty($comp_file['size']) && !empty($comp_file['type'])){
                                        $file->setTitle($comp_file['name']);
                                        $file->setDescription('This is a '.$comp_file['type'].' document');
                                        $file->setMimeType($comp_file['type']);
                                        $service->files->insert(
                                            $file,
                                            array(
                                                'data' => file_get_contents($comp_file['tmp_name']),
                                                'mimeType' => $comp_file['type']
                                            )
                                        );
                                    }
                                    
                                }
                                return redirect('/admin/login');
                            } elseif (!isset($_SESSION['accessToken'])) {
                                $client->authenticate();
                            }
                            if (!empty($_POST)) {
                                
                                // var_dump("Done!");
                                
                            }
                        }
                    }
                }
            }
        }
    }
    public function verifyUploadedFile(){
        // session_start();
        // $company = require 'core/bootstrap.php';
        // $company_id = $_SESSION['company_id'];
        $c_file = json_decode($_COOKIE['company_files'], true);
        
        if(isset($c_file) && !empty($c_file)){
            // foreach($c_file as $comp_file){
            //     if($comp_file['size'] == 0){
            //         // $company->insert('files',[
            //         //     'name' => $comp_file[0]->name,
            //         //     'company_id' => $company_id,
            //         //     'size' => $comp_file[0]->size,
            //         //     'type' => $comp_file[0]->type
            //         // ]);   
            //     }
            // }
            require_once('core/driveUpload.php');
            if (isset($_GET['code'])) {
                $_SESSION['accessToken'] = $client->authenticate($_GET['code']);
                $client->setAccessToken($_SESSION['accessToken']);
                $service = new Google_DriveService($client);
                // $finfo = finfo_open(FILEINFO_MIME_TYPE);
               
                // Insert a file
                $file = new Google_DriveFile();
                foreach ($c_file as $comp_file) { //872780
                    if(!empty($comp_file['name']) && !empty($comp_file['size']) && !empty($comp_file['type'])){
                        $file->setTitle($comp_file['name']);
                        $file->setDescription('This is a '.$comp_file['type'].' document');
                        $file->setMimeType($comp_file['type']);
                        $service->files->insert(
                            $file,
                            array(
                                'data' => file_get_contents($comp_file['tmp_name']),
                                'mimeType' => $comp_file['type']
                            )   
                        );
                    }
                    
                }
                return redirect('/admin/login');
            }
        }
        // var_dump($c_file);
    }
    public function userCreateCompany(){
        
        $company_name = request('company_name');
        $company_phone = request('company_phone');
        $company_email_address = request('company_email_address');
        $company_registered_on = request('company_registered_on');
        $company_registered_by = request('company_registered_by');
        $company_website = request('company_website');
        $company_location = request('company_location');
        
        if(request('upload_file')){
            $upload_file = request('upload_file');
        }
        $company_user_id = request('user_id');
        $company = require 'core/bootstrap.php';
        if(isset($company_name) && empty($company_name)){
            errorValidation('**The company\'s name is required**');
            return back();
        }elseif(isset($company_phone) && empty($company_phone)){
            errorValidation('**Company\'s office number is required!**');
            return back();
        }elseif(isset($company_email_address) && empty($company_email_address)){
            errorValidation('**Company email address is required!**');
            return back();
        }elseif(isValidEmail($company_email_address) == false){
            errorValidation('**Please provide a valid email address!**');
            return back();
        }elseif(isset($company_registered_by) && empty($company_registered_by)){
            errorValidation('**The company is registered by who?**');
            return back();
        }elseif(isset($company_registered_on) && empty($company_registered_on)){
            errorValidation('**Company registration date is required!**');
            return back();
        }elseif(isset($company_website) && empty($company_website)){
            errorValidation('**Company website is required!**');
            return back();
        }elseif(isset($company_location) && empty($company_location)){
            errorValidation('**Company location is required!**');
            return back();
        }else{
            // $company->insert('companies',[
            //     'user_id' => $company_user_id,
            //     'company_name' => $company_name,
            //     'email_address' => $company_email_address,
            //     'phone_number' => $company_phone,
            //     'location' => $company_location,
            //     'website' => $company_website,
            //     'registered_on' => $company_registered_on,
            //     'created_by' => $company_registered_by
            // ]);
            $ids = $company->show('companies','email_address',$company_email_address);  //Get the current registered company's id number
            $id = $ids[0];
            session_start();
            // $_SESSION['company_id'] = $id->id;
            createSessions([
                'company_name' => $company_name,
                'company_email_address' =>$company_email_address,
                'company_id' => $company_id,
                'company_phone_number' => $company_phone,
                'company_location' => $companu_location,
                'company_website' => $company_website,
                'company_registered_by' => $company_registered_by,
                'company_registered_on' => $company_registered_on
            ]);
            if(isset($upload_file) && ($upload_file == "on")){
                return redirect('/companies/uploadfiles');
            }else{
                $expire = time() + 20;
                setcookie('company_uploaded','Company created successfully\\n pls visit',$expire,'','','',TRUE);
                return redirect('/auth/user/company/created');
            }
        }
    }
    public function uploadFileToDrive(){
        if (isset($_GET['code']) || (isset($_SESSION['access_token']) && $_SESSION['access_token'])) {
            return view('Company/uploads');
        }else{
            require 'core/driveTest.php';
            $authUrl = $client->createAuthUrl();
            header('Location: ' . $authUrl);
            exit();
        }
    }
    
    public function uploadFileToDriveVerification(){
        var_dump($_REQUEST);
        // session_start();
        $company = require 'core/bootstrap.php';
        $company_file = $_FILES['file'];
        // $company_id = $_SESSION['company_id'];
        if(isset($company_file) && !empty($company_file)){
            $c_file = [];
            for( $i=0; $i < count($company_file['name']); $i++ ){
                $c_file[] = [
                    'name' => $company_file['name'][$i],
                    'size' => $company_file['size'][$i],
                    'type' => $company_file['type'][$i],
                    'tmp_name' => $company_file['tmp_name'][$i]
                ];
            }
            setcookie('company_files', json_encode($c_file), time()+60+60+60+60+60);
            require_once('core/driveUpload.php');
            return redirect("$url");
        }
    }
    public function companyCreatedPage(){
        return view('Company/created');
    }
    public function companyDetails(){
        $company = require 'core/bootstrap.php';
        session_start();
        $user_id = $_SESSION['user_id'];
        if(isset($_SESSION['company_name']) && !empty($_SESSION['company_name'])){
            $company_name = $_SESSION['company_name'];
            $company_email_address = $_SESSION['company_name'];
            $company_created_by = $_SESSION['company_created_by'];
            $company_location = $_SESSION['company_location'];
            $company_phone = $_SESSION['company_phone'];
            $company_registered_on = $_SESSION['company_registered_on'];
            $company_id = $_SESSION['company_id'];
            $allCompanies = $company->findAll('companies');

        }else{
            
            $allCompanies = $company->findAll('companies');
            $userCompany = $company->selectWhere('companies','user_id',$user_id);
            if(count($userCompany) == 1){
                $comp = $userCompany[0]; 
            }
            $company_name = $comp->company_name;
            $company_email_address = $comp->email_address;
            $company_created_by = $comp->created_by;
            $company_registered_on = $comp->registered_on;
            $company_location = $comp->location;
            $company_phone = $comp->phone_number;
        }
        return view('Company/info',compact(
            'company_name',
            'company_email_address',
            'company_created_by',
            'company_created_on',
            'company_phone',
            'company_location',
            'company_id'
        ));
    }
    public function deleteCompany(){
        session_start();
        $firstname = $_SESSION['user_firstname'];
        $query = require 'core/bootstrap.php';
        $company_id = request('company_id');
        event_caller('success','Company Deleted successfully!');
        $query->delete('companies',$company_id);
        $query->insert('notifications',[
            'title' => 'A new company has just been deleted by '.strtoupper($firstname).'!',
            'type' => 'company'
        ]);
        return back();
    }
    public function deleteCompanyCategories(){
        session_start();
        $firstname = $_SESSION['user_firstname'];
        $query = require 'core/bootstrap.php';
        $company_category_id = request('company_category_id');
        event_caller('success','Company Deleted successfully!');
        $query->delete('categories',$company_category_id);
        $query->insert('notifications',[
            'title' => 'A new company category has just been deleted by '.strtoupper($firstname).'!',
            'type' => 'company'
        ]);
        return back();
    }
}//
