<?php

class CompanyController{
    public function uploadFileToDriveVerification(){
        
        $company = require 'core/bootstrap.php';
        $company_file = $_FILES['file'];
        /**Check if the user uploaded the files successfully */
        if(isset($company_file) && !empty($company_file)){
            $c_file = [];
            for( $i=0; $i < count($company_file['name']); $i++ ){
                $c_file[] = [
                    'name' => $company_file['name'][$i],
                    'size' => $company_file['size'][$i],
                    'type' => $company_file['type'][$i],
                    'tmp_name' => $company_file['tmp_name'][$i]
                ];
            }
            /**
             * Google API calls
             */
            require 'core/driveTest.php';
            if (isset($_GET['code']) || (isset($_SESSION['access_token']) && $_SESSION['access_token'])) {
                if (isset($_GET['code'])) {
                    $client->authenticate($_GET['code']);
                    $_SESSION['access_token'] = $client->getAccessToken();
                }else{
                    $client->setAccessToken($_SESSION['access_token']);
                    $service = new Google_DriveService($client);
                    //Insert a file
                    $file = new Google_DriveFile();
                    foreach($c_file as $f_data){
                        if(!empty($f_data['name'])){
                            $file->setTitle($f_data['name']);   //Set the image title
                            $file->setDescription('This is a '.$f_data['type'].' document.');   //Set the file description by type
                            $file->setMimeType($f_data['type']);    //File type
                            $data = file_get_contents($f_data['tmp_name']);
                    
                            $createdFile = $service->files->insert($file, array(
                            'data' => $data,
                            'mimeType' => $f_data['type'],
                            'uploadType' => 'multipart'
                            ));
                            if($createdFile){

                            }
                        }
                       
                    }
                    var_dump("Files Uploaded Successfully!");
                }
            } else {
                require 'core/driveTest.php';
                $authUrl = $client->createAuthUrl();
                header('Location: ' . $authUrl);
                exit();
            }
        }
    }
    public function verify(){
        session_start();
        $user_id = $_SESSION['user_id'];
        $c_name = request('company_name');
        $c_phone = request('company_phone');
        $c_email = request('company_email_address');
        $query = require 'core/bootstrap.php';
        $c_registered_on = request('company_registered_on');
        $c_registered_by = request('company_registered_by');
        $c_website = request('company_website');
        $c_location = request('company_location');
        $c_file = request('upload_file');
        createSessions([
            'c_name' => $c_name,
            'c_email_address' => $c_email,
            'c_phone' => $c_phone,
            'c_location' => $c_location,
            'c_website' => $c_website,
            'c_registered_on' => $c_registered_on,
            'c_created_by' => $c_registered_by
        ]);
        if ($c_file == "on") {
            # code...
            // return redirect('/companies/uploadfiles');
            return redirect('/companies/uploadfiles/notice');
        }else{
            // $query->insert([
            //     'user_id' => $user_id,
            //     'company_name' => $c_name,
            //     'email_address' => $c_email,
            //     'phone_number' => $c_phone,
            //     'location' => $c_location,
            //     'website' => $c_website,
            //     'registered_on' => $c_registered_on,
            //     'created_by' => $c_registered_by
            // ]);
            return redirect('auth/user/company/created');
            
        }
    }
    public function create(){
        return view('Company/create');
    }
    public function notice(){
        return view('Company/notice');
    }
    
   
}