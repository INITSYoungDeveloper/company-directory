<?php

/**
 * User Registration
 */
class RegistrationController{
    public function userRegistration(){
        session_start();
        $user_name = callSession('user_firstname');
        if(isset($user_name) && !empty($user_name)){
            event_caller('error',"You've already signed in. You will need to sign out from the current account!");
            return redirect('/auth/home');
        }else{
             return view('auth/register'); //User registration form 
        }
    }
    public function userRegistrationStorage(){
        $query = require 'core/bootstrap.php';
        if(count($query->checkIfUserAuthExists('users','email',request('email'))) > 0){
            event_caller('error',"You have already created an account. Please login with this informations.<br> Thanks! ");
            return back();
        }else{
            $query->insert('users',[
                'firstname' => request('firstname'),
                'lastname' => request('lastname'),
                'email' => request('email'),
                'phone' => request('phone'),
                'gender' => request('gender'),
                'location' => request('location'),
                'password' => request('password'),
            ]);
            $id = $query->show('users','email',request('email'));
            $user_id = $id[0];
            session_start();
            createSessions([
                'user_firstname' => request('firstname'),
                'user_lastname' => request('lastname'),
                'user_email_address' => request('email'),
                'user_location' => request('location'),
                'user_gender' => request('gender'),
                'user_id' => $user_id->id,
                'user_phone' => request('phone')
            ]);
            return redirect('/auth/home');
        }
    }
}