<?php

class UsersController{
   
    public function index(){
        return view('User/index');
    }
    
    public function home(){
        // $date1 = "2007-03-24";
        // $date2 = "2009-06-26";
        // var_dump(intval(round(abs(strtotime($date1) - strtotime($date2))/86400)));
        session_start();
        $query = require 'core/bootstrap.php';
        if(isset($_SESSION['user_firstname'])){
            $user_id = $_SESSION['user_id'];
            $allCompanies = $query->selectAll('companies');
            $authUserCompanies = $query->selectWhere('companies','user_id',$user_id);

            return view('User/index',compact('allCompanies','authUserCompanies'));
        }else{
            event_caller('error',"You dont have access to this page. Please login with your informations.<br> Thanks! ");
            return redirect('/auth/login');
        }
        
    }
     
    public function store(){
        // event_caller('firstname_error','**Your name is required!**');
        // return back();
        echo "Yessss";
        $to      = request('email');
        $subject = 'ComCloud Account Verification';
        $message = 'Hello there please verify your account here!';
        $headers = 'From: blessingcodephp@gmail.com' . "\r\n" .
            'Reply-To: blessingcodephp@gmail.com' . "\r\n" .
            'X-Mailer: PHP/' . phpversion();

        mail($to, $subject, $message, $headers);
        exit;
        $user = require 'core/bootstrap.php';
        $fullname = request('fullname');
        $gender = request('gender');
        $phone = request('phone');
        $email_address = request('email');
        $field = request('field');
        $location = request('location');
        $password = request('password');
        $confirm_password = request('confirm_password');
        if(isset($fullname) && empty($fullname)){
            errorValidation('**Your name is required!**');
            return back();
        }elseif(isset($email_address) && empty($email_address)){
            errorValidation('**Your email address is required');
            return back();
        }elseif(isValidEmail($email_address) == false){
            errorValidation('**Please use a valid email address');
            return back();
        }elseif($password !== $confirm_password){
            errorValidation('**The two passwords doesn\'t match');
            return back();
        }elseif(isset($location) && empty($location)){
            errorValidation('**Please provide your location');
            return back();
        }elseif(count($user->checkIfUserAuthExists('users',$email_address)) > 0){
            errorValidation('User already exists. Please login!');
            return back();
        }elseif(isset($password) && empty($password)){
            errorValidation('**Password is required**');
            return back();
        }elseif(isset($phone) && empty($phone)){
            errorValidation('**Your device number is required!');
            return back();
        }else{
            $user->insert('users',[
                'fullname' => $fullname,
                'email_address' => $email_address,
                'phone' => $phone,
                'location' => $location,
                'password' => $password,
                'gender' => $gender,
                'field' => $field
            ]);
            $id = $user->show('users','email_address',$email_address);
            $user_id = $id[0];
            createSessions([
                'user_name' => $fullname,
                'user_phone' => $phone,
                'user_email_address' => $email_address,
                'user_location' => $location,
                'user_field' => $field,
                'user_gender' => $gender,
                'user_id' => $user_id->id
            ]);
            successValidation('Registration successfull');  
            return redirect('/auth/index');
        }

        
    }
    public function authenticateUserLogin(){
        $user_email_address = request('email');
        $user = require 'core/bootstrap.php';
        if(count($user->checkIfUserAuthExists('users',$user_email_address)) < 1){
            errorValidation('**You don\'t have an account yet! Please register!**');
            return back();
        }else{
            $user_details = $user->selectWhere('users','email_address',$user_email_address);
            $user_data = $user_details[0];
            session_start();
            if(isset($_SESSION['user_name']) && !empty($_SESSION['user_name'])){
                successValidation('You are logged in already');  
                return redirect('/auth/index');   
            }else{
                createSessions([
                    'user_name' => $user_data->fullname,
                    'user_phone' => $user_data->phone,
                    'user_email_address' => $user_data->email_address,
                    'user_location' => $user_data->location,
                    'user_field' => $user_data->field,
                    'user_gender' => $user_data->gender,
                    'user_id' => $user_data->id
                ]); 
                successValidation('You have successfully logged in.!'.$user_data->fullname);  
                return redirect('/auth/index');
            }
            
        }
    }
    public function authenticateUserLogOut(){
        session_start();
        session_destroy();
        successValidation('You are successfully logged out'); 
        return redirect('/auth/login');
    }
    // public function home(){
    //     session_start();
    //     if(isset($_SESSION['user_name']) && !empty($_SESSION['user_name'])){
    //         successValidation('Welcome Back '.$_SESSION['user_name'].'!');
    //         $company =  require 'core/bootstrap.php';
    //         $companies = $company->selectAll('companies');
    //         return view('User/home',compact('companies'));
    //     }else{
    //         errorValidation('**You don\'t have Access to that page. Please login!**');
    //         return redirect('/auth/login');
    //     }
        
    // }
    public function update(){

    }
    public function destroy(){

    }
}