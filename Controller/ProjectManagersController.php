<?php

class ProjectManagersController{
    public function index(){
        session_start();
        $query = require 'core/bootstrap.php';
        $project_managers = $query->selectAll('project_managers');
        return view('companies/projectManagers/index',compact('project_managers'));
    }
    public function create(){
        session_start();
        $user_id = $_SESSION['user_id'];
        $query = require 'core/bootstrap.php';
        $authorizedUsercompanies = $query->selectWhere('companies','user_id',$user_id);
        if(count($authorizedUsercompanies) < 1){
            event_caller('error','You need to create a company account before you can create a PM');
            return back();
        }else{
             return view('companies/projectManagers/create', compact('authorizedUsercompanies'));
        }
    }
    public function storePMInfo(){
        $query = require 'core/bootstrap.php';
        $query->insert('project_managers',[
            'firstname' => ucwords(request('pm_firstname')),
            'lastname' => ucwords(request('pm_lastname')),
            'email' => request('pm_email'),
            'company_id' => request('pm_company_id')
        ]);
        $query->insert('notifications',[
            'title' => 'A new Project Manager has just been added',
            'type' => 'company'
        ]);
        event_caller('success','You have successfully added ');
        return redirect('/auth/home');
    }  
    public function updatePMInfo(){
        $query = require 'core/bootstrap.php';
        $query->update('project_managers',[
            'firstname' => request('pm_firstname'),
            'lastname' => request('pm_lastname'),
            'email' => request('pm_email')
        ],request('pm_id'));
        event_caller('success','Changes made successfully');    
        return redirect('/auth/companies');
    }
    public function editPMInfo(){
        /**
         * Note: Pm stands for Project Manager
         */
        session_start();
        $query = require 'core/bootstrap.php';
        $pm_id = $_GET['no'];
        $pm = $query->selectWhere('project_managers','id',$pm_id);
        if(isset($pm) && !empty($pm)){
            $pmInfo = $pm[0];
            return view('companies/projectManagers/edit',compact('pmInfo'));
        }else{
            return redirect('/error/404');
        }
       
    }  
    public function deletePM(){
        session_start();
        $firstname = $_SESSION['user_firstname'];
        $query = require 'core/bootstrap.php';
        $pm_id = request('pm_id');
        event_caller('success','Project Manager\'s info is Deleted successfully!');
        $query->delete('project_managers',$pm_id);
        $query->insert('notifications',[
            'title' => 'A new company project manager has just been deleted by '.strtoupper($firstname).'!',
            'type' => 'project'
        ]);
        return back();
    }
}