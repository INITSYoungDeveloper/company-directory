<?php


class CompaniesDashboardController{

    public function index(){
        session_start();
        // Auth::middleware('auth');
        $id = $_GET['no'];
        $query = require 'core/bootstrap.php';
        $user_id = $_SESSION['user_id'];
        $authUserCompanies = $query->selectWhere('companies','user_id',$user_id);
        $companies = $query->selectWhere('companies','id',$id);
        if($companies){
            foreach($companies as $comp){
                $company = $comp;
            }
            $categories = $query->selectWhere('categories','company_id',$id);
            $jan = $query->selectGraph($id,'January');
            $feb = $query->selectGraph($id,'February');
            $mar = $query->selectGraph($id,'March');
            $apr = $query->selectGraph($id,'April');
            $may = $query->selectGraph($id,'May');
            $jun = $query->selectGraph($id,'June');
            $jul = $query->selectGraph($id,'July');
            $aug = $query->selectGraph($id,'August');
            $sep = $query->selectGraph($id,'September');
            $oct = $query->selectGraph($id,'Octomber');
            $nov = $query->selectGraph($id,'November');
            $dec = $query->selectGraph($id,'December');
            return view('companies/dashboard/index', compact('authUserCompanies','company','categories','jan','feb','mar','apr','may','jun','jul','aug','sep','oct','nov','dec'));
        }else{
            // event_caller('error','You have no permission to that page!');
            redirect('/error/404');
        }
        
        
    }
    
} 