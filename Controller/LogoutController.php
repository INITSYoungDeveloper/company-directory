<?php

class LogoutController{
    public function index(){
        session_start();
        $user_firstname = $_SESSION['firstname'];
        session_destroy();
        event_caller('success','Good Bye!..<br> Have a nice day!');
        return redirect('/auth/login');
    }
}