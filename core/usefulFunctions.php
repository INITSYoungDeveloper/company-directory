<?php

function view($direction,$data= []){
    extract($data);
    return require 'Views/'.$direction.'.view.php';
}
function back(){

    return header('Location: ' . $_SERVER['HTTP_REFERER']);
}
function isValidEmail($email_address){

    return (boolean)filter_var($email_address, FILTER_VALIDATE_EMAIL) && preg_match('/@.+\./', $email_address);

}
function errorValidation($message){

    $expire = time() + 20;
    setcookie('error',$message,$expire);

}
function event_caller($name,$message){
    $expire = time() + 5;
    setcookie($name,$message,$expire);
}
function successValidation($message){

    $expire = time() + 5;
   setcookie('success',$message,$expire,'','','',TRUE);

}
function redirect($location){
    return header("Location: $location");
}
function request($name){
    return $_POST[$name];
}
function createSessions($title = []){
    // session_start();
    foreach($title as $key=>$value){

        $_SESSION["$key"] = $value;
    }
    
}
function session($name){
    if(isset($_COOKIE[$name])){
        return $_COOKIE[$name];
    }
    
}
function callSession($name){
    if(isset($_SESSION["$name"])){
         echo $_SESSION["$name"];
    }
    // echo $_SESSION["$name"];
   
}
