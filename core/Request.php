<?php

class Request{
    public static function url(){
        return trim($_SERVER['PATH_INFO'],'/');
    }

    public function method(){
        return $_SERVER['REQUEST_METHOD'];
    }
    
}