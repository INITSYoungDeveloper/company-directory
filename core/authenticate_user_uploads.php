<?php

session_start();
require_once("GoogleAPI\src\Google_Client.php");
require_once("GoogleAPI\src\contrib\Google_Oauth2Service.php");
require_once("GoogleAPI\src\contrib\Google_DriveService.php");
$client = new Google_Client();
$client->setClientId('770055804012-jp0fhk9itpcltstlih7a98ftofu5i390.apps.googleusercontent.com');   //Client Id
$client->setClientSecret('6AFCaUYUDpBbGt67rQzAoEQ6');  //Client secret key
$client->setRedirectUri('http://localhost:800/companies/uploadfiles');   //The redirect uri
$client->setApplicationName("Google Drive Uploads");    //Application name
// The scope holds the auth to login and fetch the user data //
$client->setScopes('https://www.googleapis.com/auth/plus.login https://www.googleapis.com/auth/userinfo.email https://www.googleapis.com/auth/drive');
$url = $client->createAuthUrl();
$google_oauthv2 = new Google_Oauth2Service($client);