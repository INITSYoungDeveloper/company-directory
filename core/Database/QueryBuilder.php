<?php

class QueryBuilder{

    protected $pdo;

    public function __construct($pdo){     //Access the pdo instance
        $this->pdo = $pdo;
    }
    public function prepare($ag) {
        $select = $this->pdo->prepare($ag);
        $select->execute();
        return $select->fetchAll(PDO::FETCH_CLASS);
    }
    // public function 
    public function findAll($tableName, $conditions = [], $limit = []) {

        if (isset($tableName) && !empty($tableName)){ //select * from table
            
            if (isset($conditions) && !empty($conditions)) { // select * from table where condition = value

                if(isset($limit) && !empty($limit)){    // select * from table where condition = value limit 1
                    function mappers($n,$m){
                        return "$n = $m";
                    }
                    function limit($n,$m){
                        return "$n $m";
                    }
                    $array_limit = array_map("limit",array_keys($limit),array_values($limit));
                    $array_condition = array_map("mappers",array_keys($conditions),array_values($conditions));
                    $condition = implode(' AND ',$array_condition);
                    $limit = implode(' ',$array_limit);
                    $params = "SELECT * FROM $tableName WHERE $condition $limit";
                    return QueryBuilder::prepare($params);
                }else{
                    function mappers($n,$m){
                        return "$n = '$m'";
                    }
                    $array_condition = array_map("mappers",array_keys($conditions),array_values($conditions));
                    $condition = implode(' AND ',$array_condition);
                    $params = "SELECT * FROM $tableName WHERE $condition";
                    return QueryBuilder::prepare($params);
                    
                }
                
            }else{
                $params = "SELECT * FROM ".$tableName;
                return QueryBuilder::prepare($params);
            }
        } else{
           throw new Exception("Table name is not identified");
        }
    }
    public function show($table, $column, $value){
        $select = $this->pdo->prepare("SELECT id from $table where $column = '$value'");
        $select->execute();
        return $select->fetchAll(PDO::FETCH_OBJ);
    }
    public function selectAdminName($table,$email_address){

        $select = $this->pdo->prepare("SELECT firstname,lastname FROM $table where email='$email_address'");
        $select->execute();
        return $select->fetchAll(PDO::FETCH_CLASS);

    }
    public function selectNotifications($table){
        $select = $this->pdo->prepare("SELECT * FROM $table ORDER BY id DESC LIMIT 3");
        $select->execute();
        return $select->fetchAll(PDO::FETCH_CLASS);
    }
    public function selectWhere($table,$condition,$value){
        $params = "SELECT * FROM $table where $condition = '$value'";
        return QueryBuilder::prepare($params);
    }
    public function checkIfUserExists($table,$email_address){
        $select = $this->pdo->prepare("SELECT email from $table where email='$email_address'");
        $select->execute();
        return $select->fetchAll(PDO::FETCH_CLASS);
        
    }
    public function checkIfUserAuthExists($table, $condition, $value){
        $select = $this->pdo->prepare("SELECT $condition from $table where $condition='$value'");
        $select->execute();
        return $select->fetchAll(PDO::FETCH_CLASS);
        
    }
    public function loginAdmin($table,$email_address,$password){

        $select = $this->pdo->prepare("SELECT firstname,lastname,email,password FROM $table where email='$email_address' AND password='$password'");
        $select->execute();
        return $select->fetchAll(PDO::FETCH_CLASS);
        
    }
    
    public function selectCountryNames(){
        $select = $this->pdo->prepare("SELECT name from countries");
        $select->execute();
        return $select->fetchAll(PDO::FETCH_CLASS);
    }
    public function find($table, $id){
        $select = $this->pdo->prepare("SELECT * FROM $table where id= '$id'");
        $select->execute();
        return $select->fetchAll(PDO::FETCH_CLASS);
    }
    public function destroy($table,$condition, $value){
        $select = $this->pdo->prepare("DELETE FROM `$table` where $condition= '$value'");
        return $select->execute();
    }
    public function selectGraph($company_id,$value){
        $select = $this->pdo->prepare("SELECT * FROM graph where company_id='$company_id' AND month ='$value'");
        $select->execute();
        return $select->fetchAll(PDO::FETCH_CLASS);
        
    }
    public function update($table,$parameters = [],$id){
        if(isset($table) && !empty($table)){
            if(isset($parameters) && !empty($parameters)){
                function paramsValue($n,$v){
                    return "$n = '$v'";
                }
                $params = array_map("paramsValue",array_keys($parameters),array_values($parameters));
                $sql= sprintf(
                    "UPDATE %s SET %s  WHERE id= %s",
    
                    $table,

                    implode(',',$params),

                    $id

                );
                try{

                    $statement= $this->pdo->prepare("$sql");
    
                    return $statement->execute($parameters);
    
                }catch(PDOException $e){
    
                    die($e->getMessage());
    
                }
           }else{
               throw new Exception("Please provide the insert data name");
           }
        }
    }
    public function selectAll($table){  //Selects any data with the argument passed
        $select = $this->pdo->prepare("SELECT * FROM $table");
        $select->execute();
        return $select->fetchAll(PDO::FETCH_CLASS);
    }
    public function insert($table,$parameters){
        if(isset($table) && !empty($table)){
            if(isset($parameters) && !empty($parameters)){
                $sql= sprintf(
    
                    "INSERT INTO %s(%s) VALUES(%s)",
                    $table,                                                     //First value: Table name
                    implode(',',array_keys($parameters)),                       //Second value: columns names
                    '"'.implode('","',array_values($parameters)).'"'            //Third value: columns value

                );

                try{
                    $statement= $this->pdo->prepare("$sql");
                    return $statement->execute($parameters);
                }catch(PDOException $e){
    
                    die($e->getMessage());
    
                }
           }else{
               throw new Exception("Please provide the insert data name");
           }
        }else{
            throw new Exception("Please provide the table name to save the data");
        }

       

    }
    public function selectJoin($id){
        $select = "SELECT business_categories.category from business_categories join businesses ON business_categories.business_id = $id GROUP BY business_categories.category";
        $query = $this->pdo->prepare($select);
        $query->execute();
        $fetch = $query->fetchAll(PDO::FETCH_CLASS) ;
        return $fetch;
    }
    public function selectLike($tableName,$condition = []){
        if(isset($tableName) && !empty($tableName)){

            if(isset($condition) && !empty($condition)){
                function mapps($n,$m){
                    return "`$n` LIKE '%$m%'";
                }
                $likes = array_map("mapps",array_keys($condition),array_values($condition));
                $data = implode(" OR ", $likes);
                $select = "SELECT * FROM $tableName where $data";
                // var_dump($select);
                // exit;
                $query = $this->pdo->prepare($select);
                $query->execute();
                return $query->fetchAll(PDO::FETCH_CLASS);
                
            }
        }else{
            throw new Exception("No table Name Defined!Pleasee provide the target");
        }
    }
    public function delete($table, $id){
        if(isset($table) && !empty($table)){
            if(isset($id) && !empty($id)){
                try{
                    $deleteQuery = "DELETE FROM $table WHERE id='$id'";
                    $query = $this->pdo->prepare($deleteQuery);
                    return $query->execute();
                }catch(Exception $e){
                    die($e->getMessage());
                }
                
            }else{
                throw new Exception("Your information is required for this task!");
            }
        }else{
            throw new Exception("Your table name is required!");
        }
    }
    public function selectIssues($project_id,$title){
         $select = "SELECT * FROM issues WHERE project_id='$project_id' AND title='$title'";
         $query = $this->pdo->prepare($select);
        $query->execute();
        $fetch = $query->fetchAll(PDO::FETCH_CLASS) ;
        return $fetch;

    }
}