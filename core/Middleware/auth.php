<?php  

class Auth{
    // protected $data;
    public static function middleware($type){
        if($type == 'auth'){
            if(!isset($_SESSION['user_firstname']) OR empty($_SESSION['user_firstname'])){
                errorValidation('You need to be authenticated, you have no access to that page!');
                return redirect('/auth/login');
            }
        }elseif($type == 'login'){
            if(isset($_SESSION['user_firstname']) && !empty($_SESSION['user_firstname'])){
                event_caller('error','You have logged in already. Want to login to another account? <a class="text-primary" href="/auth/logout">LOGOUT</a>');
                return redirect('/auth/home');
            }else{
                return redirect('/auth/login');
            //    event_caller('error','You');
            }
        }
        
    }
}